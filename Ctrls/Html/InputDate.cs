﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Web;
    using Utils;
    using Utils.Types;

    /// <summary>
    /// Allows programmatic access to the fully globalized HTML5 &lt;input type="date" /&gt; equivalent element on the server.
    /// </summary>
    [ToolboxData("<{0}:InputDate runat='server' />")]
    public sealed class InputDate : InputText
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputDate"/> class.
        /// </summary>
        public InputDate()
        {
            Attributes["data-type"] = "date";
        }

        /// <summary>
        /// Gets or sets the minimum value.
        /// </summary>
        /// <remarks>
        /// The default value is "1899-12-30" (OLE Automation compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("1899-12-30")]
        public string Min
        {
            get
            {
                return Attributes["data-min"] ?? "1899-12-30";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    Date min;
                    if(Date.TryParseExact(value, Config.IsoDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out min))
                    {
                        Date max;
                        if(Date.TryParseExact(Max, Config.IsoDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out max) && min > max)
                            throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                Attributes["data-min"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        /// <remarks>
        /// The default value is "2030-12-31" (OLE Automation compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("2030-12-31")]
        public string Max
        {
            get
            {
                return Attributes["data-max"] ?? "2030-12-31";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    Date max;
                    if(Date.TryParseExact(value, Config.IsoDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out max))
                    {
                        Date min;
                        if(Date.TryParseExact(Min, Config.IsoDate, CultureInfo.InvariantCulture, DateTimeStyles.None, out min) && min > max)
                            throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                Attributes["data-max"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            if(MaxLength == -1)
                MaxLength = 50;
            Attributes.Remove("data-min");
            Attributes.Remove("data-max");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public override void CreateValidators()
        {
            base.CreateValidators();
            int index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                var rv = new RangeValidator
                {
                    ControlToValidate = ID,
                    CultureInvariantValues = true,
                    Type = ValidationDataType.Date,
                    MinimumValue = Min,
                    MaximumValue = Max,
                    ErrorMessage = string.Format(CultureInfo.CurrentCulture, "{1:d}{0}≤ {3} ≤{0}{2:d}", Environment.NewLine, Date.ParseExact(Min, Config.IsoDate, CultureInfo.InvariantCulture), Date.ParseExact(Max, Config.IsoDate, CultureInfo.InvariantCulture), HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, ValidationDataType.Date.GetName(), CultureInfo.CurrentCulture)),
                    Display = ValidatorDisplay.Dynamic,
                    SetFocusOnError = true,
                    CssClass = "Validator",
                    ValidationGroup = vg.Trim(),
                    ViewStateMode = ViewStateMode.Disabled
                };
                rv.ApplyStyleSheetSkin(Page);
                Parent.Controls.AddAt(++index, rv);
                rv.ID = rv.UniqueID.Substring(rv.NamingContainer.UniqueID.Length + 1);
            }
            var page = (BasePage)Page;
            if(!page.IsStartupScriptRegistered("DatePicker"))
            {
                string lang = null, specific = CultureInfo.CurrentCulture.Name, neutral = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                switch(neutral)
                {
                    case "af":
                    case "az":
                    case "bg":
                    case "bs":
                    case "ca":
                    case "cs":
                    case "da":
                    case "de":
                    case "el":
                    case "eo":
                    case "es":
                    case "et":
                    case "eu":
                    case "fa":
                    case "fi":
                    case "fo":
                    case "gl":
                    case "he":
                    case "hi":
                    case "hr":
                    case "hu":
                    case "hy":
                    case "id":
                    case "is":
                    case "it":
                    case "ja":
                    case "ka":
                    case "kk":
                    case "km":
                    case "ko":
                    case "lb":
                    case "lt":
                    case "lv":
                    case "mk":
                    case "ml":
                    case "ms":
                    case "no":
                    case "pl":
                    case "rm":
                    case "ro":
                    case "ru":
                    case "sk":
                    case "sl":
                    case "sq":
                    case "sv":
                    case "ta":
                    case "th":
                    case "tj":
                    case "tr":
                    case "uk":
                    case "vi":
                    {
                        lang = neutral;
                        break;
                    }
                    case "ar":
                    {
                        lang = specific == "ar-DZ" ? specific : neutral;
                        break;
                    }
                    case "cy":
                    {
                        lang = "cy-GB";
                        break;
                    }
                    case "en":
                    {
                        if(specific == "en-AU" || specific == "en-GB" || specific == "en-NZ")
                            lang = specific;
                        break;
                    }
                    case "fr":
                    {
                        lang = specific == "fr-CH" ? specific : neutral;
                        break;
                    }
                    case "nl":
                    {
                        lang = specific == "nl-BE" ? specific : neutral;
                        break;
                    }
                    case "pt":
                    {
                        lang = specific == "pt-BR" ? specific : neutral;
                        break;
                    }
                    case "sr":
                    {
                        lang = specific.Contains("Cyrl") ? neutral : "sr-SR";
                        break;
                    }
                    case "zh":
                    {
                        lang = specific == "zh-HK" || specific == "zh-TW" ? specific : "zh-CN";
                        break;
                    }
                }
                page.RegisterStartupScript("jQuery-UI", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='http{0}://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui{1}.js' onerror=\"this.onerror = null; this.src = '{2}jQuery/jquery-ui{1}.js';\"></script>", Page.Request.IsSecureConnection ? "s" : null, !Context.IsDebuggingEnabled ? ".min" : null, Properties.Settings.Default.ScriptsPath));
                if(lang != null)
                    page.RegisterStartupScript("DatePicker", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='http{0}://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/i18n/jquery.ui.datepicker-{1}{2}.js' onerror=\"this.onerror = null; this.src = '{3}jQuery/i18n/jquery.ui.datepicker-{1}{2}.js';\"></script><script type='text/javascript'>$.datepicker.setDefaults(page.DatePicker);</script>", Page.Request.IsSecureConnection ? "s" : null, lang, !Context.IsDebuggingEnabled ? ".min" : null, Properties.Settings.Default.ScriptsPath));
                else
                    page.RegisterStartupScript("DatePicker", "<script type='text/javascript'>$.datepicker.setDefaults(page.DatePicker);</script>");
            }
            Parent.Controls.AddAt(++index, new RegisterScript(string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>$('#{0}').datepicker({{ dateFormat: \"{1}\", minDate: new Date('{2}'), maxDate: new Date('{3}'), yearRange: '{4}:{5}', shortYearCutoff: {6}, constrainInput: false }});</script>", ClientID, CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Replace("yyyy", "yy").Replace("M", "m"), Min, Max, Min.Substring(0, 4), Max.Substring(0, 4), CultureInfo.CurrentCulture.Calendar.TwoDigitYearMax - 2000)));
        }
    }
}
