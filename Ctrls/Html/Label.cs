﻿namespace Ctrls.Html
{
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Allows programmatic access to the HTML &lt;label /&gt; element on the server.
    /// </summary>
    [ToolboxData("<{0}:Label runat='server' />")]
    public sealed class Label : HtmlGenericControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.
        /// </summary>
        public Label() : base("label")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Label"/> class.
        /// </summary>
        /// <param name="tag">Ignored (inheritance), always "label".</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "tag")]
        // ReSharper disable once UnusedParameter.Local
        public Label(string tag) : base("label")
        {
        }

        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip title text.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the identifier for an associated server control.
        /// </summary>
        [TypeConverter(typeof(AssociatedControlConverter))]
        [IDReferenceProperty]
        [Themeable(false)]
        [Category("Accessibility")]
        public string For
        {
            get
            {
                return Attributes["for"] ?? string.Empty;
            }
            set
            {
                Attributes["for"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            if(!string.IsNullOrWhiteSpace(For))
                For = (Parent.FindControl(For) ?? NamingContainer.FindControl(For)).ClientID;
            base.RenderAttributes(writer);
        }
    }
}
