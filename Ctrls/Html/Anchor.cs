﻿namespace Ctrls.Html
{
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    /// <summary>
    /// Allows programmatic access to the HTML &lt;a /&gt; element on the server.
    /// </summary>
    /// <remarks>
    /// Leaves the "HRef" attribute as is, and does not resolve a relative URL.
    /// </remarks>
    [ToolboxData("<{0}:Anchor runat='server' />")]
    public sealed class Anchor : HtmlContainerControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Anchor"/> class.
        /// </summary>
        public Anchor() : base("a")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Anchor"/> class.
        /// </summary>
        /// <param name="tag">Ignored (inheritance), always "a".</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "tag")]
        // ReSharper disable once UnusedParameter.Local
        public Anchor(string tag) : base("a")
        {
        }

        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the name of the browser window or frame that displays the contents of the web page that is linked to when this control is clicked.
        /// </summary> 
        [Category("Navigation")]
        public string Target
        {
            get
            {
                return Attributes["target"] ?? string.Empty;
            }
            set
            {
                Attributes["target"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the relationship between the current URL and the linked URL.
        /// </summary> 
        [Category("Navigation")]
        public string Rel
        {
            get
            {
                return Attributes["rel"] ?? string.Empty;
            }
            set
            {
                Attributes["rel"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the URL target of the link specified in this server control.
        /// </summary>
        [Category("Navigation")]
        public string HRef
        {
            get
            {
                return Attributes["href"] ?? string.Empty;
            }
            set
            {
                Attributes["href"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }
    }
}
