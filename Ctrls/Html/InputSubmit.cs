namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Utils;

    /// <summary>
    /// Allows programmatic access to the HTML &lt;input type="submit" /&gt; element on the server.
    /// </summary> 
    /// <remarks>
    /// Removes the "ValidationGroup" attribute from the rendered output, to pass the strict HTML markup validation.
    /// </remarks>
    [ToolboxData("<{0}:InputSubmit runat='server' />")]
    public sealed class InputSubmit : HtmlInputSubmit
    {
        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        private string _commandName;

        /// <summary>
        /// Gets or sets the command name associated with this control that is passed to the <see cref="Command"/> event.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string CommandName
        {
            get
            {
                return _commandName ?? string.Empty;
            }
            set
            {
                _commandName = value;
            }
        }

        private string _commandArgument;

        /// <summary>
        /// Gets or sets an optional parameter passed to the <see cref="Command"/> event along with the associated <see cref="CommandName"/>.
        /// </summary>
        [Bindable(true)]
        [Themeable(false)]
        [Category("Behavior")]
        public string CommandArgument
        {
            get
            {
                return _commandArgument ?? string.Empty;
            }
            set
            {
                _commandArgument = value;
            }
        }

        private static readonly object EventCommand = new object();

        /// <summary>
        /// Occurs when this control is clicked.
        /// </summary>
        [Category("Action")]
        public event CommandEventHandler Command
        {
            add
            {
                Events.AddHandler(EventCommand, value);
            }
            remove
            {
                Events.RemoveHandler(EventCommand, value);
            }
        }

        private void OnCommand(CommandEventArgs e)
        {
            var handler = Events[EventCommand] as CommandEventHandler;
            if(handler != null)
                handler(this, e);
            RaiseBubbleEvent(this, e);
        }

        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        /// <inheritdoc/>
        protected override object SaveControlState()
        {
            var cs = new object[4];
            cs[0] = base.SaveControlState();
            cs[1] = _commandName;
            cs[2] = _commandArgument;
            cs[3] = ValidationGroup;
            return cs;
        }

        /// <inheritdoc/>
        protected override void LoadControlState(object savedState)
        {
            if(savedState != null)
            {
                var cs = (object[])savedState;
                base.LoadControlState(cs[0]);
                _commandName = (string)cs[1];
                _commandArgument = (string)cs[2];
                ValidationGroup = (string)cs[3];
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            EnsureID();
            bool causes = CausesValidation;
            string group = ValidationGroup;
            if(causes)
                Attributes["onclick"] = Helper.CombineJavaScript(Attributes["onclick"], string.Format(CultureInfo.InvariantCulture, "Page_ClientValidate('{0}');", group));
            CausesValidation = false;
            Attributes.Remove("ValidationGroup");
            base.RenderAttributes(writer);
            CausesValidation = causes;
            ValidationGroup = group;
        }

        /// <inheritdoc/>
        protected override void RaisePostBackEvent(string eventArgument)
        {
            base.RaisePostBackEvent(eventArgument);
            OnCommand(new CommandEventArgs(CommandName, CommandArgument));
        }
    }
}
