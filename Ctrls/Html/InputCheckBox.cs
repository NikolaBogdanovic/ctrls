﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Ctrls.Web;
    using Utils;

    /// <summary>
    /// Allows programmatic access to the HTML &lt;input type="checkbox" /&gt; element on the server.
    /// </summary>
    [ValidationProperty("Checked")]
    [ToolboxData("<{0}:InputCheckBox runat='server' />")]
    public sealed class InputCheckBox : HtmlInputCheckBox, IDynamicValidation
    {
        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control is read-only.
        /// </summary>
        [Category("Behavior")]
        public bool ReadOnly
        {
            get
            {
                return Attributes["readonly"] == "readonly";
            }
            set
            {
                Attributes["readonly"] = value ? "readonly" : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the HTML5 server control is required.
        /// </summary>
        [Category("Behavior")]
        public bool Required
        {
            get
            {
                return Attributes["required"] == "required";
            }
            set
            {
                Attributes["required"] = value ? "required" : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the option selection.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool AutoPostBack
        {
            get
            {
                return ViewState["AutoPostBack"] as bool? ?? false;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether validation is performed when this control posts back.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CausesValidation
        {
            get
            {
                return ViewState["CausesValidation"] as bool? ?? false;
            }
            set
            {
                ViewState["CausesValidation"] = value;
            }
        }

        /// <inheritdoc/>
        [TypeConverter(typeof(StringArrayConverter))]
        [Themeable(false)]
        [Category("Behavior")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public string[] ValidationGroup
        {
            get
            {
                var value = ViewState["ValidationGroup"] as string[];
                if(value != null)
                    return (string[])value.Clone();
                return new[] { string.Empty };
            }
            set
            {
                ViewState["ValidationGroup"] = value != null && value.Length > 0 ? value.Clone() : null;
            }
        }

        /// <inheritdoc/>
        protected override void RaisePostDataChangedEvent()
        {
            if(AutoPostBack && !Page.IsPostBackEventControlRegistered)
            {
                Page.AutoPostBackControl = this;
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        Page.Validate(vg.Trim());
                }
            }
            base.RaisePostDataChangedEvent();
        }

        private static readonly object EventDataBound = new object();

        /// <summary>
        /// Occurs after the server control binds to a data source.
        /// </summary>
        [Category("Data")]
        public event EventHandler DataBound
        {
            add
            {
                Events.AddHandler(EventDataBound, value);
            }
            remove
            {
                Events.RemoveHandler(EventDataBound, value);
            }
        }

        private void OnDataBound(EventArgs e)
        {
            var handler = Events[EventDataBound] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <inheritdoc/>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnDataBound(e);
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            EnsureID();
            if(AutoPostBack)
            {
                string onchange = Attributes["onchange"];
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        onchange = Helper.CombineJavaScript(onchange, string.Format(CultureInfo.InvariantCulture, "if(!Page_ClientValidate('{0}')) return false;", vg.Trim()));
                }
                Attributes["onchange"] = Helper.CombineJavaScript(onchange, "__doPostBack(this.name, '');");
            }
            Attributes.Remove("AutoPostBack");
            Attributes.Remove("CausesValidation");
            Attributes.Remove("ValidationGroup");
            Attributes.Remove("required");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public void CreateValidators()
        {
            int index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                if(Required)
                {
                    var cv = new CheckedValidator
                    {
                        ControlToValidate = ID,
                        ErrorMessage = Title,
                        Display = ValidatorDisplay.Dynamic,
                        SetFocusOnError = true,
                        CssClass = "Validator",
                        ValidationGroup = vg.Trim(),
                        ViewStateMode = ViewStateMode.Disabled
                    };
                    cv.ApplyStyleSheetSkin(Page);
                    Parent.Controls.AddAt(++index, cv);
                    cv.ID = cv.UniqueID.Substring(cv.NamingContainer.UniqueID.Length + 1);
                }
            }
        }
    }
}
