namespace Ctrls.Html
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Reflection;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Utils;

    /// <summary>
    /// Allows programmatic access to the HTML &lt;select /&gt; element on the server.
    /// </summary>
    /// <remarks>
    /// Corrects the asynchronous or late data-binding.
    /// <para></para>
    /// Corrects the "Size" attribute, to pass the strict HTML markup validation.
    /// <para></para>
    /// Remembers all HTML &lt;option /&gt; element attributes between postbacks.
    /// </remarks>
    [ToolboxData("<{0}:Select runat='server' />")]
    [ControlValueProperty("SelectedValue")]
    [ValidationProperty("SelectedValue")]
    public sealed class Select : HtmlSelect, IDynamicValidation
    {
        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the HTML server control is required.
        /// </summary>
        [Category("Behavior")]
        public bool Required
        {
            get
            {
                return Attributes["required"] == "required";
            }
            set
            {
                Attributes["required"] = value ? "required" : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the option selection.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool AutoPostBack
        {
            get
            {
                return ViewState["AutoPostBack"] as bool? ?? false;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether validation is performed when this control posts back.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CausesValidation
        {
            get
            {
                return ViewState["CausesValidation"] as bool? ?? false;
            }
            set
            {
                ViewState["CausesValidation"] = value;
            }
        }

        /// <inheritdoc/>
        [TypeConverter(typeof(StringArrayConverter))]
        [Themeable(false)]
        [Category("Behavior")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public string[] ValidationGroup
        {
            get
            {
                var value = ViewState["ValidationGroup"] as string[];
                if(value != null)
                    return (string[])value.Clone();
                return new[] { string.Empty };
            }
            set
            {
                ViewState["ValidationGroup"] = value != null && value.Length > 0 ? value.Clone() : null;
            }
        }

        /// <summary>
        /// Gets or sets the height (in rows) of this control.
        /// </summary>
        public new int Size
        {
            get
            {
                string s = Attributes["size"];
                if(!string.IsNullOrWhiteSpace(s))
                    return int.Parse(s, CultureInfo.InvariantCulture);
                if(Multiple)
                    return 4;
                return 1;
            }
            set
            {
                Attributes["size"] = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        private string _dataTitleField;

        /// <summary>
        /// Gets or sets the field from the data source to bind to the title property of each item in this control.
        /// </summary>
        [Category("Data")]
        public string DataTitleField
        {
            get
            {
                return _dataTitleField ?? string.Empty;
            }
            set
            {
                _dataTitleField = value;
                OnDataPropertyChanged();
            }
        }

        private string _dataTitleFormatString;

        /// <summary>
        /// Gets or sets the formatting string used to control how data bound to the list control is displayed.
        /// </summary>
        [Themeable(false)]
        [Category("Data")]
        public string DataTitleFormatString
        {
            get
            {
                return _dataTitleFormatString ?? string.Empty;
            }
            set
            {
                _dataTitleFormatString = value;
                OnDataPropertyChanged();
            }
        }

        private string _dataTextFormatString;

        /// <summary>
        /// Gets or sets the formatting string used to control how data bound to the list control is displayed.
        /// </summary>
        [Themeable(false)]
        [Category("Data")]
        public string DataTextFormatString
        {
            get
            {
                return _dataTextFormatString ?? string.Empty;
            }
            set
            {
                _dataTextFormatString = value;
                OnDataPropertyChanged();
            }
        }

        private string _dataValueFormatString;

        /// <summary>
        /// Gets or sets the formatting string used to control how data bound to the list control is displayed.
        /// </summary>
        [Themeable(false)]
        [Category("Data")]
        public string DataValueFormatString
        {
            get
            {
                return _dataValueFormatString ?? string.Empty;
            }
            set
            {
                _dataValueFormatString = value;
                OnDataPropertyChanged();
            }
        }

        private int _cachedSelectedIndex = -1;

        /// <inheritdoc/>
        [Themeable(false)]
        [Bindable(true)]
        [Category("Behaviour")]
        public override int SelectedIndex
        {
            get
            {
                for(int i = 0; i < Items.Count; i++)
                {
                    if(Items[i].Selected)
                        return i;
                }
                if(Multiple || Size > 1 || Items.Count == 0)
                    return -1;
                ListItem li = Items[0];
                li.Selected = true;
                if(!string.IsNullOrWhiteSpace(li.Attributes["title"]))
                    Title = li.Attributes["title"];
                return 0;
            }
            set
            {
                if(Items.Count > 0)
                {
                    ClearSelection();
                    if(value != -1)
                    {
                        ListItem li = Items[value];
                        li.Selected = true;
                        if(!string.IsNullOrWhiteSpace(li.Attributes["title"]))
                            Title = li.Attributes["title"];
                    }
                    _cachedSelectedIndex = -1;
                }
                else
                    _cachedSelectedIndex = value;
            }
        }

        /// <summary>
        /// Gets the selected item with the lowest index in the list control.
        /// </summary> 
        /// <returns>
        /// The lowest indexed item selected from the list control.
        /// </returns>
        [Category("Behavior")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ListItem SelectedItem
        {
            get
            {
                int selectedIndex = SelectedIndex;
                if(selectedIndex != -1)
                    return Items[selectedIndex];
                return null;
            }
        }

        private string _cachedSelectedValue;

        /// <summary>
        /// Gets or sets the value of the selected item in this control or sets the <see cref="SelectedIndex"/> property of the control to the index of the first item in the list with the specified value.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Bindable(true, BindingDirection.TwoWay)]
        [Browsable(false)]
        [Themeable(false)]
        [Category("Behavior")]
        public string SelectedValue
        {
            get
            {
                ListItem li = SelectedItem;
                if(li != null)
                    return li.Value;
                return null;
            }
            set
            {
                if(Items.Count > 0)
                {
                    ClearSelection();
                    if(!string.IsNullOrWhiteSpace(value))
                    {
                        ListItem li = Items.FindByValue(value);
                        if(li != null)
                        {
                            li.Selected = true;
                            if(!string.IsNullOrWhiteSpace(li.Attributes["title"]))
                                Title = li.Attributes["title"];
                        }
                    }
                    _cachedSelectedValue = null;
                }
                else
                    _cachedSelectedValue = value;
            }
        }

        private string _cachedSelectedText;

        /// <summary>
        /// Gets or sets the text of the selected item in this control or sets the <see cref="SelectedIndex"/> property of the control to the index of the first item in the list with the specified text.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Bindable(true, BindingDirection.TwoWay)]
        [Browsable(false)]
        [Themeable(false)]
        [Category("Behavior")]
        public string SelectedText
        {
            get
            {
                ListItem li = SelectedItem;
                if(li != null)
                    return li.Text;
                return null;
            }
            set
            {
                if(Items.Count > 0)
                {
                    ClearSelection();
                    if(!string.IsNullOrWhiteSpace(value))
                    {
                        ListItem li = Items.FindByText(value);
                        if(li != null)
                        {
                            li.Selected = true;
                            if(!string.IsNullOrWhiteSpace(li.Attributes["title"]))
                                Title = li.Attributes["title"];
                        }
                    }
                    _cachedSelectedText = null;
                }
                else
                    _cachedSelectedText = value;
            }
        }

        /// <summary>
        /// Gets or sets the value of the selected item in this control or sets the <see cref="SelectedIndex"/> property of the control to the index of the first item in the list with the specified value.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Use the SelectedValue property instead.", true)]
        public new string Value
        {
            get
            {
                return SelectedValue;
            }
            set
            {
                SelectedValue = value;
            }
        }

        private static readonly object EventDataBound = new object();

        /// <summary>
        /// Occurs after the server control binds to a data source.
        /// </summary>
        [Category("Data")]
        public event EventHandler DataBound
        {
            add
            {
                Events.AddHandler(EventDataBound, value);
            }
            remove
            {
                Events.RemoveHandler(EventDataBound, value);
            }
        }

        private void OnDataBound(EventArgs e)
        {
            var handler = Events[EventDataBound] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        private delegate void BaseBaseOnDataBinding(EventArgs e);

        /// <inheritdoc/>
        protected override void OnDataBinding(EventArgs e)
        {
            using(var c = new Control())
            {
                var del = (BaseBaseOnDataBinding)Delegate.CreateDelegate(typeof(BaseBaseOnDataBinding), c, "OnDataBinding");
                // ReSharper disable PossibleNullReferenceException
                del.GetType().BaseType.BaseType.GetField("_target", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(del, this);
                // ReSharper restore PossibleNullReferenceException
                del(e);
            }
            IEnumerable data = GetData();
            if(data != null)
            {
                var collection = data as ICollection;
                if(collection != null)
                    Items.Capacity = Items.Count + collection.Count;
                string dataTextField = DataTextField;
                string dataTextFormatString = DataTextFormatString;
                string dataValueField = DataValueField;
                string dataValueFormatString = DataValueFormatString;
                string dataTitleField = DataTitleField;
                string dataTitleFormatString = DataTitleFormatString;
                bool flag1 = dataTextField.Length > 0, flag2 = dataValueField.Length > 0, flag3 = dataTitleField.Length > 0;
                if(flag1 || flag2 || flag3)
                {
                    foreach(object container in data)
                    {
                        var item = new ListItem();
                        if(flag1)
                            item.Text = DataBinder.GetPropertyValue(container, dataTextField, dataTextFormatString);
                        if(flag2)
                            item.Value = DataBinder.GetPropertyValue(container, dataValueField, dataValueFormatString);
                        if(flag3)
                            item.Attributes["title"] = DataBinder.GetPropertyValue(container, dataTitleField, dataTitleFormatString);
                        Items.Add(item);
                    }
                }
                else
                {
                    flag1 = dataTextFormatString.Length > 0;
                    flag2 = dataValueFormatString.Length > 0;
                    flag3 = dataTitleFormatString.Length > 0;
                    if(flag1 || flag2 || flag3)
                    {
                        foreach(object container in data)
                        {
                            var item = new ListItem();
                            if(flag1)
                                item.Text = string.Format(CultureInfo.CurrentCulture, dataTextFormatString, container);
                            if(flag2)
                                item.Value = string.Format(CultureInfo.CurrentCulture, dataValueFormatString, container);
                            if(flag3)
                                item.Attributes["title"] = string.Format(CultureInfo.CurrentCulture, dataTitleFormatString, container);
                            Items.Add(item);
                        }
                    }
                    else
                    {
                        foreach(object container in data)
                            Items.Add(container.ToString());
                    }
                }
            }
            if(_cachedSelectedIndex != -1)
            {
                int index = _cachedSelectedIndex;
                SelectedIndex = _cachedSelectedIndex;
                if((_cachedSelectedValue != null && _cachedSelectedValue != Items[index].Value) || (_cachedSelectedText != null && _cachedSelectedText != Items[index].Text))
                    throw new ArgumentException("Argument Exception: Attributes mutually exclusive: SelectedIndex, SelectedValue, SelectedText");
                _cachedSelectedValue = _cachedSelectedText = null;
            }
            else if(_cachedSelectedValue != null)
            {
                SelectedValue = _cachedSelectedValue;
                if(_cachedSelectedText != null && _cachedSelectedText != Items[SelectedIndex].Text)
                    throw new ArgumentException("Argument Exception: Attributes mutually exclusive: SelectedIndex, SelectedValue, SelectedText");
                _cachedSelectedText = null;
                _cachedSelectedIndex = -1;
            }
            else if(_cachedSelectedText != null)
            {
                SelectedText = _cachedSelectedText;
                _cachedSelectedValue = null;
                _cachedSelectedIndex = -1;
            }
            ViewState["_!DataBound"] = true;
            RequiresDataBinding = false;
            OnDataBound(EventArgs.Empty);
        }

        /// <inheritdoc/>
        protected override object SaveViewState()
        {
            int i = 0;
            var vs = new object[Items.Count + 1];
            vs[i++] = base.SaveViewState();
            foreach(ListItem li in Items)
            {
                int j = 0;
                var attribs = new KeyValuePair<string, string>[li.Attributes.Count];
                foreach(string key in li.Attributes.Keys)
                    attribs[j++] = new KeyValuePair<string, string>(key, li.Attributes[key]);
                vs[i++] = attribs;
            }
            return vs;
        }

        /// <inheritdoc/>
        protected override void LoadViewState(object savedState)
        {
            if(savedState != null)
            {
                int i = 0;
                var vs = (object[])savedState;
                base.LoadViewState(vs[i++]);
                foreach(ListItem li in Items)
                {
                    foreach(KeyValuePair<string, string> kvp in (KeyValuePair<string, string>[])vs[i++])
                        li.Attributes[kvp.Key] = kvp.Value;
                }
            }
        }

        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            base.OnInit(e);
        }

        /// <inheritdoc/>
        protected override object SaveControlState()
        {
            var cs = new object[8];
            cs[0] = base.SaveControlState();
            cs[1] = _cachedSelectedIndex;
            cs[2] = _cachedSelectedValue;
            cs[3] = _cachedSelectedText;
            cs[4] = _dataTextFormatString;
            cs[5] = _dataValueFormatString;
            cs[6] = _dataTitleFormatString;
            cs[7] = _dataTitleField;
            return cs;
        }

        /// <inheritdoc/>
        protected override void LoadControlState(object savedState)
        {
            if(savedState != null)
            {
                var cs = (object[])savedState;
                base.LoadControlState(cs[0]);
                _cachedSelectedIndex = (int)cs[1];
                _cachedSelectedValue = (string)cs[2];
                _cachedSelectedText = (string)cs[3];
                _dataTextFormatString = (string)cs[4];
                _dataValueFormatString = (string)cs[5];
                _dataTitleFormatString = (string)cs[6];
                _dataTitleField = (string)cs[7];
            }
        }

        private void RenderTitle()
        {
            if(!Multiple && Size == 1)
                Title = SelectedItem.Attributes["title"];
        }

        /// <inheritdoc/>
        protected override void OnServerChange(EventArgs e)
        {
            RenderTitle();
            base.OnServerChange(e);
        }

        /// <inheritdoc/>
        protected override void RaisePostDataChangedEvent()
        {
            if(AutoPostBack && !Page.IsPostBackEventControlRegistered)
            {
                Page.AutoPostBackControl = this;
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        Page.Validate(vg.Trim());
                }
            }
            base.RaisePostDataChangedEvent();
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            EnsureID();
            if(Items.Count > 0)
                RenderTitle();
            else
                Attributes.Remove("title");
            string onchange = Helper.CombineJavaScript("if(!this.multiple && this.size == 1) this.title = this.options[this.selectedIndex].title;", Attributes["onchange"]);
            if(AutoPostBack)
            {
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        onchange = Helper.CombineJavaScript(onchange, string.Format(CultureInfo.InvariantCulture, "if(!Page_ClientValidate('{0}')) return false;", vg.Trim()));
                }
                onchange = Helper.CombineJavaScript(onchange, "__doPostBack(this.name, '');");
            }
            Attributes["onchange"] = onchange;
            Attributes.Remove("AutoPostBack");
            Attributes.Remove("CausesValidation");
            Attributes.Remove("ValidationGroup");
            Attributes.Remove("required");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public void CreateValidators()
        {
            if(Required)
            {
                int index = Parent.Controls.IndexOf(this);
                foreach(string vg in ValidationGroup)
                {
                    var rfv = new RequiredFieldValidator
                    {
                        ControlToValidate = ID,
                        ErrorMessage = (string)HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, "required", CultureInfo.CurrentCulture),
                        Display = ValidatorDisplay.Dynamic,
                        SetFocusOnError = true,
                        CssClass = "Validator",
                        ValidationGroup = vg.Trim(),
                        ViewStateMode = ViewStateMode.Disabled
                    };
                    rfv.ApplyStyleSheetSkin(Page);
                    Parent.Controls.AddAt(++index, rfv);
                    rfv.ID = rfv.UniqueID.Substring(rfv.NamingContainer.UniqueID.Length + 1);
                }
            }
        }
    }
}
