﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Web;
    using Utils;

    /// <summary>
    /// Allows programmatic access to the fully globalized HTML5 &lt;input type="number-double" /&gt; equivalent element on the server.
    /// </summary>
    [ToolboxData("<{0}:InputNumberDouble runat='server' />")]
    public sealed class InputNumberDouble : InputText
    {
        private const NumberStyles Float = NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowExponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="InputNumberDouble"/> class.
        /// </summary>
        public InputNumberDouble()
        {
            Attributes["data-type"] = "number-double";
            Attributes["data-step"] = "any";
        }

        /// <summary>
        /// Gets or sets the HTML5 "step" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "any". Cannot be in exponential notation.
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("any")]
        public string Step
        {
            get
            {
                return Attributes["data-step"] ?? "any";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    value = value.Trim();
                    if(value != "any")
                    {
                        double d;
                        if(!double.TryParse(value, NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d) || d <= 0 || 79228162514264337593543950335d < d)
                            throw new ArgumentOutOfRangeException("value", value, @"0 < Double <= 79228162514264337593543950335");
                        if(value[0] == '.')
                            value = "0" + value;
                        else if(value[value.Length - 1] == '.')
                            value = value + "0";
                    }
                }
                else
                    value = null;
                Attributes["data-step"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "min" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "-79228162514264337593543950335" (Decimal compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("-79228162514264337593543950335")]
        public string Min
        {
            get
            {
                return Attributes["data-min"] ?? "-79228162514264337593543950335";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    double min;
                    if(!double.TryParse(value, Float, CultureInfo.InvariantCulture, out min) || min < -79228162514264337593543950335d || 79228162514264337593543950335d < min)
                        throw new ArgumentOutOfRangeException("value", value, @"-79228162514264337593543950335 <= Double <= 79228162514264337593543950335");
                    if(double.Parse(Max, Float, CultureInfo.InvariantCulture) < min)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-min"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "max" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "79228162514264337593543950335" (Decimal compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("79228162514264337593543950335")]
        public string Max
        {
            get
            {
                return Attributes["data-max"] ?? "79228162514264337593543950335";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    double max;
                    if(!double.TryParse(value, Float, CultureInfo.InvariantCulture, out max) || max < -79228162514264337593543950335d || 79228162514264337593543950335d < max)
                        throw new ArgumentOutOfRangeException("value", value, @"-79228162514264337593543950335 <= Double <= 79228162514264337593543950335");
                    if(double.Parse(Min, Float, CultureInfo.InvariantCulture) > max)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-max"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            if(MaxLength == -1)
                MaxLength = 35;
            if(string.IsNullOrWhiteSpace(Pattern))
            {
                NumberFormatInfo format = CultureInfo.CurrentCulture.NumberFormat;
                Pattern = string.Format(CultureInfo.InvariantCulture, @"[0-9 (){0}{1}{2}{3}{4}]{5}", format.CurrencyDecimalSeparator, format.CurrencyDecimalSeparator != format.NumberDecimalSeparator ? format.NumberDecimalSeparator : null, format.CurrencyDecimalSeparator != format.PercentDecimalSeparator && format.NumberDecimalSeparator != format.PercentDecimalSeparator ? format.PercentDecimalSeparator : null, format.PositiveSign, format.NegativeSign, Required ? '+' : '*');
            }
            Attributes.Remove("data-min");
            Attributes.Remove("data-max");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public override void CreateValidators()
        {
            if(string.IsNullOrWhiteSpace(Placeholder))
                Placeholder = 0.ToString("F", CultureInfo.CurrentCulture);
            base.CreateValidators();
            int index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                var rv = new RangeValidator
                {
                    ControlToValidate = ID,
                    CultureInvariantValues = true,
                    Type = ValidationDataType.Double,
                    MinimumValue = Min,
                    MaximumValue = Max,
                    ErrorMessage = string.Format(CultureInfo.CurrentCulture, "{1:R}{0}≤ {3} ≤{0}{2:R}", Environment.NewLine, double.Parse(Min, Float, CultureInfo.InvariantCulture), double.Parse(Max, Float, CultureInfo.InvariantCulture), HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, ValidationDataType.Double.GetName(), CultureInfo.CurrentCulture)),
                    Display = ValidatorDisplay.Dynamic,
                    SetFocusOnError = true,
                    CssClass = "Validator",
                    ValidationGroup = vg.Trim(),
                    ViewStateMode = ViewStateMode.Disabled
                };
                rv.ApplyStyleSheetSkin(Page);
                Parent.Controls.AddAt(++index, rv);
                rv.ID = rv.UniqueID.Substring(rv.NamingContainer.UniqueID.Length + 1);
            }
            ((BasePage)Page).RegisterStartupScript("NumericSpinner", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}NumericSpinner{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
            Parent.Controls.AddAt(++index, new RegisterScript(string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>NumericSpinner('{0}');</script>", ClientID)));
        }
    }
}
