﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Web;
    using Utils;

    /// <summary>
    /// Allows programmatic access to the fully globalized HTML5 &lt;input type="number-currency" /&gt; equivalent element on the server.
    /// </summary>
    [ToolboxData("<{0}:InputNumberCurrency runat='server' />")]
    public sealed class InputNumberCurrency : InputText
    {
        private const NumberStyles Number = NumberStyles.AllowLeadingSign | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowExponent;

        /// <summary>
        /// Initializes a new instance of the <see cref="InputNumberCurrency"/> class.
        /// </summary>
        public InputNumberCurrency()
        {
            Attributes["data-type"] = "number-currency";
            Attributes["data-step"] = Config.ServerCurrency.LowestDenomination.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Gets or sets the HTML5 "step" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is server currency lowest denomination. Cannot be in exponential notation, or "any".
        /// </remarks>
        [Category("Behavior")]
        public string Step
        {
            get
            {
                return Attributes["data-step"] ?? Config.ServerCurrency.LowestDenomination.ToString(CultureInfo.InvariantCulture);
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal m;
                    if(!decimal.TryParse(value, NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out m) || m <= 0 || 9007199254740992 < m)
                        throw new ArgumentOutOfRangeException("value", value, @"0 < Decimal <= 9007199254740992");
                    if(value[0] == '.')
                        value = "0" + value;
                    else if(value[value.Length - 1] == '.')
                        value = value + "0";
                }
                else
                    value = null;
                Attributes["data-step"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "min" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "-9007199254740992" (JavaScript compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("-9007199254740992")]
        public string Min
        {
            get
            {
                return Attributes["data-min"] ?? "-9007199254740992";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal min;
                    if(!decimal.TryParse(value, Number, CultureInfo.InvariantCulture, out min) || min < -9007199254740992 || 9007199254740992 < min)
                        throw new ArgumentOutOfRangeException("value", value, @"-9007199254740992 <= Decimal <= 9007199254740992");
                    if(decimal.Parse(Max, Number, CultureInfo.InvariantCulture) < min)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-min"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "max" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "9007199254740992" (JavaScript compatible).
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("9007199254740992")]
        public string Max
        {
            get
            {
                return Attributes["data-max"] ?? "9007199254740992";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal max;
                    if(!decimal.TryParse(value, Number, CultureInfo.InvariantCulture, out max) || max < -9007199254740992 || 9007199254740992 < max)
                        throw new ArgumentOutOfRangeException("value", value, @"-9007199254740992 <= Decimal <= 9007199254740992");
                    if(decimal.Parse(Min, Number, CultureInfo.InvariantCulture) > max)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-max"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            if(MaxLength == -1)
                MaxLength = 25;
            if(string.IsNullOrWhiteSpace(Pattern))
            {
                NumberFormatInfo format = CultureInfo.CurrentCulture.NumberFormat;
                Pattern = string.Format(CultureInfo.InvariantCulture, @"[0-9 (){0}{1}{2}{3}{4}{5}{6}{7}]{8}", format.CurrencyGroupSeparator, format.CurrencyGroupSeparator != format.NumberGroupSeparator ? format.NumberGroupSeparator : null, format.CurrencyGroupSeparator != format.PercentGroupSeparator && format.NumberGroupSeparator != format.PercentGroupSeparator ? format.PercentGroupSeparator : null, format.CurrencyDecimalSeparator, format.CurrencyDecimalSeparator != format.NumberDecimalSeparator ? format.NumberDecimalSeparator : null, format.CurrencyDecimalSeparator != format.PercentDecimalSeparator && format.NumberDecimalSeparator != format.PercentDecimalSeparator ? format.PercentDecimalSeparator : null, format.PositiveSign, format.NegativeSign, Required ? '+' : '*');
            }
            Attributes.Remove("data-min");
            Attributes.Remove("data-max");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public override void CreateValidators()
        {
            string format = Step;
            int i = format.IndexOf('.');
            if(++i != 0)
                i = format.Length - i;
            format = "N" + i.ToString(CultureInfo.InvariantCulture);
            if(string.IsNullOrWhiteSpace(Placeholder))
                Placeholder = 0.ToString(format, CultureInfo.CurrentCulture);
            base.CreateValidators();
            var index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                var rv = new ForeignCurrencyRangeValidator
                {
                    ControlToValidate = ID,
                    CultureInvariantValues = true,
                    MinimumValue = Min,
                    MaximumValue = Max,
                    ErrorMessage = string.Format(CultureInfo.CurrentCulture, "{1}{0}≤ {3} ≤{0}{2}", Environment.NewLine, decimal.Parse(Min, Number, CultureInfo.InvariantCulture).ToString(format, CultureInfo.CurrentCulture), decimal.Parse(Max, Number, CultureInfo.InvariantCulture).ToString(format, CultureInfo.CurrentCulture), HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, ValidationDataType.Currency.GetName(), CultureInfo.CurrentCulture)),
                    Display = ValidatorDisplay.Dynamic,
                    SetFocusOnError = true,
                    CssClass = "Validator",
                    ValidationGroup = vg.Trim(),
                    ViewStateMode = ViewStateMode.Disabled
                };
                rv.ApplyStyleSheetSkin(Page);
                Parent.Controls.AddAt(++index, rv);
                rv.ID = rv.UniqueID.Substring(rv.NamingContainer.UniqueID.Length + 1);
            }
            ((BasePage)Page).RegisterStartupScript("NumericSpinner", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}NumericSpinner{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
            Parent.Controls.AddAt(++index, new RegisterScript(string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>NumericSpinner('{0}');</script>", ClientID)));
        }
    }
}
