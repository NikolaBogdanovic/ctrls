﻿namespace Ctrls.Html
{
    /// <summary>
    /// Defines methods that HTML server controls must implement to automatically create dynamic validators.
    /// </summary>
    public interface IDynamicValidation
    {
        /// <summary>
        /// Gets or sets an array that contains the groups of controls for which this control causes validation when it posts back to the server.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        string[] ValidationGroup
        {
            get;
            set;
        }

        /// <summary>
        /// Dynamically creates the validators for this control.
        /// </summary>
        void CreateValidators();
    }
}
