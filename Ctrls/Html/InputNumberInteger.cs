﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Web;
    using Utils;

    /// <summary>
    /// Allows programmatic access to the fully globalized HTML5 &lt;input type="number-integer" /&gt; equivalent element on the server.
    /// </summary>
    [ToolboxData("<{0}:InputNumberInteger runat='server' />")]
    public sealed class InputNumberInteger : InputText
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputNumberInteger"/> class.
        /// </summary>
        public InputNumberInteger()
        {
            Attributes["data-type"] = "number-integer";
            Attributes["data-step"] = "1";
        }

        /// <summary>
        /// Gets or sets the HTML5 "step" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "1". Cannot be in exponential notation, or "any".
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("1")]
        public string Step
        {
            get
            {
                return Attributes["data-step"] ?? "1";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    int i;
                    if(!int.TryParse(value, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out i) || i <= 0)
                        throw new ArgumentOutOfRangeException("value", value, @"0 < Integer");
                }
                else
                    value = null;
                Attributes["data-step"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "min" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "-2147483648". Cannot be in exponential notation.
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("-2147483648")]
        public string Min
        {
            get
            {
                return Attributes["data-min"] ?? "-2147483648";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    int min;
                    if(!int.TryParse(value, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out min))
                        throw new ArgumentOutOfRangeException("value", value, @"Integer");
                    if(int.Parse(Max, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture) < min)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-min"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the HTML5 "max" equivalent value.
        /// </summary>
        /// <remarks>
        /// The default value is "2147483647". Cannot be in exponential notation.
        /// </remarks>
        [Category("Behavior")]
        [DefaultValue("2147483647")]
        public string Max
        {
            get
            {
                return Attributes["data-max"] ?? "2147483647";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    int max;
                    if(!int.TryParse(value, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out max))
                        throw new ArgumentOutOfRangeException("value", value, @"Integer");
                    if(int.Parse(Min, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture) > max)
                        throw new ArgumentOutOfRangeException("value", value, @"Min <= Max");
                }
                else
                    value = null;
                Attributes["data-max"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            if(MaxLength == -1)
                MaxLength = 15;
            if(string.IsNullOrWhiteSpace(Pattern))
            {
                NumberFormatInfo format = CultureInfo.CurrentCulture.NumberFormat;
                Pattern = string.Format(CultureInfo.InvariantCulture, @"[0-9 (){0}{1}]{2}", format.PositiveSign, format.NegativeSign, Required ? '+' : '*');
            }
            Attributes.Remove("data-min");
            Attributes.Remove("data-max");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public override void CreateValidators()
        {
            if(string.IsNullOrWhiteSpace(Placeholder))
                Placeholder = 0.ToString("D", CultureInfo.CurrentCulture);
            base.CreateValidators();
            int index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                var rv = new RangeValidator
                {
                    ControlToValidate = ID,
                    CultureInvariantValues = true,
                    Type = ValidationDataType.Integer,
                    MinimumValue = Min,
                    MaximumValue = Max,
                    ErrorMessage = string.Format(CultureInfo.CurrentCulture, "{1:D}{0}≤ {3} ≤{0}{2:D}", Environment.NewLine, int.Parse(Min, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture), int.Parse(Max, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture), HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, ValidationDataType.Integer.GetName(), CultureInfo.CurrentCulture)),
                    Display = ValidatorDisplay.Dynamic,
                    SetFocusOnError = true,
                    CssClass = "Validator",
                    ValidationGroup = vg.Trim(),
                    ViewStateMode = ViewStateMode.Disabled
                };
                rv.ApplyStyleSheetSkin(Page);
                Parent.Controls.AddAt(++index, rv);
                rv.ID = rv.UniqueID.Substring(rv.NamingContainer.UniqueID.Length + 1);
            }
            ((BasePage)Page).RegisterStartupScript("NumericSpinner", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}NumericSpinner{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
            Parent.Controls.AddAt(++index, new RegisterScript(string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>NumericSpinner('{0}');</script>", ClientID)));
        }
    }
}
