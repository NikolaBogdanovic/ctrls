﻿namespace Ctrls.Html
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Utils;

    /// <summary>
    /// Allows programmatic access to the HTML5 &lt;input type="email|password|search|tel|text|url" /&gt; element on the server.
    /// </summary>
    /// <remarks>
    /// Removes the "ValidationGroup" attribute from the rendered output, to pass the strict HTML markup validation.
    /// </remarks>
    [ToolboxData("<{0}:InputText runat='server' />")]
    public class InputText : HtmlInputText, IEditableTextControl, IDynamicValidation
    {
        /// <inheritdoc/>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Use the Value property instead.", true)]
        public string Text
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the CSS class.
        /// </summary>
        [Category("Appearance")]
        public string Class
        {
            get
            {
                return Attributes["class"] ?? string.Empty;
            }
            set
            {
                Attributes["class"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Title
        {
            get
            {
                return Attributes[@"title"] ?? string.Empty;
            }
            set
            {
                Attributes[@"title"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the placeholder hint.
        /// </summary>
        [Category("Appearance")]
        [Localizable(true)]
        public string Placeholder
        {
            get
            {
                return Attributes[@"placeholder"] ?? string.Empty;
            }
            set
            {
                Attributes[@"placeholder"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets the regular expression pattern.
        /// </summary>
        [Category("Behavior")]
        public string Pattern
        {
            get
            {
                return Attributes["pattern"] ?? string.Empty;
            }
            set
            {
                Attributes["pattern"] = !string.IsNullOrWhiteSpace(value) ? value : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control is read-only.
        /// </summary>
        [Category("Behavior")]
        public bool ReadOnly
        {
            get
            {
                return Attributes["readonly"] == "readonly";
            }
            set
            {
                Attributes["readonly"] = value ? "readonly" : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the HTML5 server control is required.
        /// </summary>
        [Category("Behavior")]
        public bool Required
        {
            get
            {
                return Attributes["required"] == "required";
            }
            set
            {
                Attributes["required"] = value ? "required" : null;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a postback to the server automatically occurs when the user changes the option selection.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool AutoPostBack
        {
            get
            {
                return ViewState["AutoPostBack"] as bool? ?? false;
            }
            set
            {
                ViewState["AutoPostBack"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether validation is performed when this control posts back.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CausesValidation
        {
            get
            {
                return ViewState["CausesValidation"] as bool? ?? false;
            }
            set
            {
                ViewState["CausesValidation"] = value;
            }
        }

        /// <inheritdoc/>
        [TypeConverter(typeof(StringArrayConverter))]
        [Themeable(false)]
        [Category("Behavior")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1819:PropertiesShouldNotReturnArrays")]
        public string[] ValidationGroup
        {
            get
            {
                var value = ViewState["ValidationGroup"] as string[];
                if(value != null)
                    return (string[])value.Clone();
                return new[] { string.Empty };
            }
            set
            {
                ViewState["ValidationGroup"] = value != null && value.Length > 0 ? value.Clone() : null;
            }
        }

        private static readonly object EventTextChanged = new object();

        /// <inheritdoc/>
        [Category("Action")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Use the ServerChange event instead.", true)]
        public event EventHandler TextChanged
        {
            add
            {
                Events.AddHandler(EventTextChanged, value);
            }
            remove
            {
                Events.RemoveHandler(EventTextChanged, value);
            }
        }

        private void OnTextChanged(EventArgs e)
        {
            var handler = Events[EventTextChanged] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <inheritdoc/>
        protected override void RaisePostDataChangedEvent()
        {
            if(AutoPostBack && !Page.IsPostBackEventControlRegistered)
            {
                Page.AutoPostBackControl = this;
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        Page.Validate(vg.Trim());
                }
            }
            base.RaisePostDataChangedEvent();
            OnTextChanged(EventArgs.Empty);
        }

        private static readonly object EventDataBound = new object();

        /// <summary>
        /// Occurs after the server control binds to a data source.
        /// </summary>
        [Category("Data")]
        public event EventHandler DataBound
        {
            add
            {
                Events.AddHandler(EventDataBound, value);
            }
            remove
            {
                Events.RemoveHandler(EventDataBound, value);
            }
        }

        private void OnDataBound(EventArgs e)
        {
            var handler = Events[EventDataBound] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <inheritdoc/>
        protected override void OnDataBinding(EventArgs e)
        {
            base.OnDataBinding(e);
            OnDataBound(e);
        }

        /// <inheritdoc/>
        protected override void RenderAttributes(HtmlTextWriter writer)
        {
            EnsureID();
            if(MaxLength == -1)
                MaxLength = 256;
            string onchange = Helper.CombineJavaScript("if(window.AutoComplete) clearTimeout(AutoComplete); this.previousValue = this.value;", Attributes["onchange"]);
            if(AutoPostBack)
            {
                if(CausesValidation)
                {
                    foreach(string vg in ValidationGroup)
                        onchange = Helper.CombineJavaScript(onchange, string.Format(CultureInfo.InvariantCulture, "if(!Page_ClientValidate('{0}')) return false;", vg.Trim()));
                }
                onchange = Helper.CombineJavaScript(onchange, "__doPostBack(this.name, '');");
            }
            Attributes["onchange"] = onchange;
            Attributes["onfocus"] = "this.onfocus = null; this.previousValue = this.value;";
            Attributes["onblur"] = Helper.CombineJavaScript(Attributes["onblur"], "if(this.value != this.previousValue) { var that = this; window.AutoComplete = setTimeout(function() { AutoComplete = null; TriggerChange(that) }, 100); }");
            Attributes.Remove("AutoPostBack");
            Attributes.Remove("CausesValidation");
            Attributes.Remove("ValidationGroup");
            Attributes.Remove("required");
            Attributes.Remove("pattern");
            base.RenderAttributes(writer);
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public virtual void CreateValidators()
        {
            if(Type == "email")
            {
                if(string.IsNullOrWhiteSpace(Title))
                    Title = @"username@example.com";
                if(string.IsNullOrWhiteSpace(Placeholder))
                    Placeholder = @"username@example.com";
                if(string.IsNullOrWhiteSpace(Pattern))
                    Pattern = @"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
            }
            int index = Parent.Controls.IndexOf(this);
            foreach(string vg in ValidationGroup)
            {
                if(Required)
                {
                    var rfv = new RequiredFieldValidator
                    {
                        ControlToValidate = ID,
                        ErrorMessage = (string)HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, "required", CultureInfo.CurrentCulture),
                        Display = ValidatorDisplay.Dynamic,
                        SetFocusOnError = true,
                        CssClass = "Validator",
                        ValidationGroup = vg.Trim(),
                        ViewStateMode = ViewStateMode.Disabled
                    };
                    rfv.ApplyStyleSheetSkin(Page);
                    Parent.Controls.AddAt(++index, rfv);
                    rfv.ID = rfv.UniqueID.Substring(rfv.NamingContainer.UniqueID.Length + 1);
                }
                if(!string.IsNullOrWhiteSpace(Pattern))
                {
                    var rev = new RegularExpressionValidator
                    {
                        ControlToValidate = ID,
                        ErrorMessage = Title,
                        Display = ValidatorDisplay.Dynamic,
                        SetFocusOnError = true,
                        CssClass = "Validator",
                        ValidationExpression = Pattern,
                        ValidationGroup = vg.Trim(),
                        ViewStateMode = ViewStateMode.Disabled
                    };
                    rev.ApplyStyleSheetSkin(Page);
                    Parent.Controls.AddAt(++index, rev);
                    rev.ID = rev.UniqueID.Substring(rev.NamingContainer.UniqueID.Length + 1);
                }
            }
        }
    }
}
