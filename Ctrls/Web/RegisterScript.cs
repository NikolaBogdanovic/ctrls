﻿namespace Ctrls.Web
{
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web.UI;

    /// <summary>
    /// Registers a startup HTML &lt;script /&gt; block.
    /// </summary>
    /// <remarks>
    /// Executes on full postback if it's visible, or on asynchronous postback if it's visible and inside an updating <see cref="System.Web.UI.UpdatePanel"/>.
    /// </remarks>
    [ToolboxData("<{0}:RegisterScript runat='server'><script type='text/javascript'></script></{0}:RegisterScript>")]
    public sealed class RegisterScript : Control
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterScript"/> class.
        /// </summary>
        public RegisterScript()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegisterScript"/> class.
        /// </summary>
        /// <param name="script">The script to register.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public RegisterScript(string script)
        {
            Controls.Add(new LiteralControl(script));
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override void Render(HtmlTextWriter writer)
        {
            var sb = new StringBuilder();
            base.Render(new HtmlTextWriter(new StringWriter(sb, CultureInfo.InvariantCulture)));
            if(!string.IsNullOrWhiteSpace(sb.ToString()))
            {
                ScriptManager script = ScriptManager.GetCurrent(Page);
                if(script != null && script.SupportsPartialRendering)
                    ScriptManager.RegisterStartupScript(this, typeof(RegisterScript), ClientID, sb.ToString(), false);
                else
                    Page.ClientScript.RegisterStartupScript(typeof(RegisterScript), ClientID, sb.ToString(), false);
            }
        }
    }
}
