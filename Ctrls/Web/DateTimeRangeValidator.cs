namespace Ctrls.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Utils;

    /// <summary>
    /// Checks whether the date and time value of an input control is within a specified range of values.
    /// </summary>
    /// <remarks>
    /// Supports client validation, unobtrusive JavaScript, all <see cref="System.DateTime"/> formats, and is fully globalized and time zone aware.
    /// </remarks>
    [ToolboxData("<{0}:DateTimeRangeValidator runat='server' />")]
    public sealed class DateTimeRangeValidator : CustomValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeRangeValidator"/> class.
        /// </summary>
        public DateTimeRangeValidator()
        {
            ClientValidationFunction = "DateTimeRange";
            ServerValidate += DateTimeRange;
        }

        /// <summary>
        /// Gets the data type that the values being compared are converted to before the comparison is made.
        /// </summary>
        [Category("Behavior")]
        public static readonly Type Type = typeof(DateTime);

        /// <summary>
        /// Gets or sets a value indicating whether values are converted to a culture-neutral format before being compared.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CultureInvariantValues
        {
            get
            {
                return ViewState["CultureInvariantValues"] as bool? ?? false;
            }
            set
            {
                ViewState["CultureInvariantValues"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum value of the validation range, in server time zone.
        /// </summary>
        /// <remarks>
        /// The default value is "1899-12-30T00:00:00" (OLE Automation compatible), in server time zone.
        /// </remarks>
        [DefaultValue("1899-12-30T00:00:00")]
        [Themeable(false)]
        [Category("Behavior")]
        public string MinimumValueInServerTimeZone
        {
            get
            {
                return ViewState["MinimumValueInServerTimeZone"] as string ?? "1899-12-30T00:00:00";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime min;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out min))
                    {
                        if(min.Kind == DateTimeKind.Utc)
                            min = G11n.ToServerTimeZone(min);
                        if(DateTime.ParseExact(MaximumValueInServerTimeZone, "s", CultureInfo.InvariantCulture, DateTimeStyles.None) < min)
                            throw new ArgumentOutOfRangeException("value", value, @"MinimumValueInServerTimeZone <= MaximumValueInServerTimeZone");
                        value = min.ToString("s", CultureInfo.InvariantCulture);
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["MinimumValueInServerTimeZone"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum value of the validation range, in client time zone.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string MinimumValue
        {
            get
            {
                return G11n.ToClientTimeZone(DateTime.ParseExact(MinimumValueInServerTimeZone, "s", CultureInfo.InvariantCulture)).ToString("s", CultureInfo.InvariantCulture);
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime min;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out min))
                    {
                        min = G11n.ToServerTimeZone(min);
                        if(DateTime.ParseExact(MaximumValueInServerTimeZone, "s", CultureInfo.InvariantCulture, DateTimeStyles.None) < min)
                            throw new ArgumentOutOfRangeException("value", value, @"MinimumValue <= MaximumValue");
                        value = min.ToString("s", CultureInfo.InvariantCulture);
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["MinimumValueInServerTimeZone"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value of the validation range, in server time zone.
        /// </summary>
        /// <remarks>
        /// The default value is "2030-12-31T23:59:59" (OLE Automation compatible), in server time zone.
        /// </remarks>
        [Themeable(false)]
        [Category("Behavior")]
        public string MaximumValueInServerTimeZone
        {
            get
            {
                return ViewState["MaximumValueInServerTimeZone"] as string ?? "2030-12-31T23:59:59";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime max;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out max))
                    {
                        if(max.Kind == DateTimeKind.Utc)
                            max = G11n.ToServerTimeZone(max);
                        if(DateTime.ParseExact(MinimumValueInServerTimeZone, "s", CultureInfo.InvariantCulture, DateTimeStyles.None) > max)
                            throw new ArgumentOutOfRangeException("value", value, @"MinimumValueInServerTimeZone <= MaximumValueInServerTimeZone");
                        value = max.ToString("s", CultureInfo.InvariantCulture);
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["MaximumValueInServerTimeZone"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value of the validation range, in client time zone.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string MaximumValue
        {
            get
            {
                return G11n.ToClientTimeZone(DateTime.ParseExact(MaximumValueInServerTimeZone, "s", CultureInfo.InvariantCulture)).ToString("s", CultureInfo.InvariantCulture);
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime max;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out max))
                    {
                        max = G11n.ToServerTimeZone(max);
                        if(DateTime.ParseExact(MinimumValueInServerTimeZone, "s", CultureInfo.InvariantCulture, DateTimeStyles.None) > max)
                            throw new ArgumentOutOfRangeException("value", value, @"MinimumValue <= MaximumValue");
                        value = max.ToString("s", CultureInfo.InvariantCulture);
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["MaximumValueInServerTimeZone"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(!RenderUplevel)
                return;
            var page = (BasePage)Page;
            if(!page.IsStartupScriptRegistered("DateTimePatterns"))
            {
                var custom = new List<string>(132);
                var standard = new[] { 'd', 'g', 's', 'G', 'D', 'f', 'F' };
                for(int i = 0, j = standard.Length; i < j; i++)
                    custom.AddRange(CultureInfo.CurrentCulture.DateTimeFormat.GetAllDateTimePatterns(standard[i]));
                page.RegisterStartupScript("DateTimePatterns", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>var DateTimePatterns = [\"{0}\"];</script>", string.Join("\", \"", custom)));
            }
            page.RegisterStartupScript("DateTimeValidation", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}DateTimeValidation{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
        }

        /// <inheritdoc/>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            EnsureID();
            base.AddAttributesToRender(writer);
            if(!RenderUplevel)
                return;
            if(writer != null && IsUnobtrusive)
            {
                writer.AddAttribute("data-val-minimumvalue", MinimumValue, false);
                writer.AddAttribute("data-val-maximumvalue", MaximumValue, false);
            }
            else
            {
                ScriptManager script = ScriptManager.GetCurrent(Page);
                if(script != null && script.SupportsPartialRendering)
                {
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "minimumvalue", MinimumValue, false);
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "maximumvalue", MaximumValue, false);
                }
                else
                {
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "minimumvalue", MinimumValue, false);
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "maximumvalue", MaximumValue, false);
                }
            }
        }

        private static void DateTimeRange(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = false;
            DateTime val;
            if(DateTime.TryParse(e.Value, CultureInfo.CurrentCulture, DateTimeStyles.None, out val))
            {
                var rv = (DateTimeRangeValidator)sender;
                e.IsValid = DateTime.ParseExact(rv.MinimumValue, "s", rv.CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture) <= val && val <= DateTime.ParseExact(rv.MaximumValue, "s", rv.CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture);
            }
        }
    }
}
