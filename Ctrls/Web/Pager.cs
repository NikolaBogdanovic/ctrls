﻿namespace Ctrls.Web
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Html;
    using Utils;

    /// <summary>
    /// Provides paging functionality for asynchronous or late data-bound controls.
    /// </summary>
    [SupportsEventValidation]
    [ParseChildren(false)]
    [PersistChildren(true)]
    [Themeable(true)]
    [ToolboxData("<{0}:Pager runat='server' />")]
    public sealed class Pager : WebControl, INamingContainer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pager"/> class.
        /// </summary>
        public Pager() : base(HtmlTextWriterTag.Div)
        {
            Visible = false;
        }

        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            if(QueryStringHandled)
                return;
            string query = QueryStringValue;
            if(!string.IsNullOrWhiteSpace(query))
            {
                int page;
                if(!int.TryParse(query, NumberStyles.None, CultureInfo.InvariantCulture, out page) || page < 1)
                    throw new HttpException(404, "404 Not Found");
                StartRowIndex = (page - 1) * MaximumRows;
            }
            QueryStringHandled = true;
            base.OnInit(e);
        }

        /// <summary>
        /// Gets or sets the index of the first record that is displayed on a page of data.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int StartRowIndex
        {
            get
            {
                return ViewState["StartRowIndex"] as int? ?? 0;
            }
            set
            {
                Contract.Requires(value >= 0);

                ViewState["StartRowIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum number of records that are displayed for each page of data.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int MaximumRows
        {
            get
            {
                return ViewState["MaximumRows"] as int? ?? Config.PageSize;
            }
            set
            {
                Contract.Requires(value > 0);

                ViewState["MaximumRows"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the total number of records that are retrieved by the underlying data source object that is referenced by the associated data-bound control.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int TotalRowCount
        {
            get
            {
                return ViewState["TotalRowCount"] as int? ?? -1;
            }
            set
            {
                Contract.Requires(value >= -1);

                ViewState["TotalRowCount"] = value;
                Visible = value != -1;
                if(ChildControlsCreated)
                    ChildControlsCreated = false;
                EnsureChildControls();
            }
        }

        /// <summary>
        /// Gets the size of each page of data.
        /// </summary>
        [Category("Paging")]
        public int PageSize
        {
            get
            {
                return MaximumRows;
            }
        }

        /// <summary>
        /// Gets the number of current page of data.
        /// </summary>
        [Category("Paging")]
        public int CurrentPage
        {
            get
            {
                if(TotalRowCount > 0)
                    return (StartRowIndex / MaximumRows) + 1;
                return 1;
            }
        }

        /// <summary>
        /// Gets the count of all pages of data.
        /// </summary>
        [Category("Paging")]
        public int PageCount
        {
            get
            {
                if(TotalRowCount > 0)
                    return ((TotalRowCount - 1) / MaximumRows) + 1;
                return 1;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the query string field has been evaluated.
        /// </summary>
        private bool QueryStringHandled
        {
            get
            {
                return ViewState["QueryStringHandled"] as bool? ?? false;
            }
            set
            {
                ViewState["QueryStringHandled"] = value;
            }
        }

        /// <summary>
        /// Gets the value of the query string field from the URL of the request.
        /// </summary>
        private string QueryStringValue
        {
            get
            {
                return Page.Request.QueryString[QueryStringField];
            }
        }

        /// <summary>
        /// Gets or sets a URL that contains the query string field.
        /// </summary>
        public string QueryStringNavigateUrl
        {
            get
            {
                if(ViewState["QueryStringNavigateUrl"] == null)
                {
                    NameValueCollection qs = HttpUtility.ParseQueryString(Page.Request.QueryString.ToString(), Page.Request.ContentEncoding);
                    qs.Set(QueryStringField, "{0}");
                    ViewState["QueryStringNavigateUrl"] = Link.Absolute(string.Format(CultureInfo.InvariantCulture, "{0}?{1}", Page.Request.Url.AbsolutePath, qs), Page.Request.IsSecureConnection);
                }
                return (string)ViewState["QueryStringNavigateUrl"];
            }
            set
            {
                ViewState["QueryStringNavigateUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the query string field.
        /// </summary>
        [Category("Paging")]
        public string QueryStringField
        {
            get
            {
                return (string)ViewState["QueryStringField"] ?? string.Empty;
            }
            set
            {
                ViewState["QueryStringField"] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether validation is performed when this control posts back. Always true.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public static bool CausesValidation
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the group of controls for which this control causes validation when it posts back to the server.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string ValidationGroup
        {
            get
            {
                return (string)ViewState["ValidationGroup"] ?? string.Empty;
            }
            set
            {
                ViewState["ValidationGroup"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the first page.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string FirstPageCssClass
        {
            get
            {
                return (string)ViewState["FirstPageCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["FirstPageCssClass"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the previous page.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string PreviousPageCssClass
        {
            get
            {
                return (string)ViewState["PreviousPageCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["PreviousPageCssClass"] = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the next page.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string NextPageCssClass
        {
            get
            {
                return (string)ViewState["NextPageCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["NextPageCssClass"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the last page.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string LastPageCssClass
        {
            get
            {
                return (string)ViewState["LastPageCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["LastPageCssClass"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the page numbers.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string PageNumbersCssClass
        {
            get
            {
                return (string)ViewState["PageNumbersCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["PageNumbersCssClass"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the cascading style sheet (CSS) class that is used to style the current page number.
        /// </summary>
        [CssClassProperty]
        [Category("Appearance")]
        public string CurrentPageCssClass
        {
            get
            {
                return (string)ViewState["CurrentPageCssClass"] ?? string.Empty;
            }
            set
            {
                ViewState["CurrentPageCssClass"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the page number input is displayed.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool ShowPages
        {
            get
            {
                return ViewState["ShowPages"] as bool? ?? true;
            }
            set
            {
                ViewState["ShowPages"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the rows number input is displayed.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool ShowRows
        {
            get
            {
                return ViewState["ShowRows"] as bool? ?? true;
            }
            set
            {
                ViewState["ShowRows"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the first page is displayed.
        /// </summary>
        [Category("Behavior")]
        public bool ShowFirstPage
        {
            get
            {
                return ViewState["ShowFirstPage"] as bool? ?? false;
            }
            set
            {
                ViewState["ShowFirstPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the last page is displayed.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool ShowPreviousPage
        {
            get
            {
                return ViewState["ShowPreviousPage"] as bool? ?? true;
            }
            set
            {
                ViewState["ShowPreviousPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the next page is displayed.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool ShowNextPage
        {
            get
            {
                return ViewState["ShowNextPage"] as bool? ?? true;
            }
            set
            {
                ViewState["ShowNextPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the last page is displayed.
        /// </summary>
        [Category("Behavior")]
        public bool ShowLastPage
        {
            get
            {
                return ViewState["ShowLastPage"] as bool? ?? false;
            }
            set
            {
                ViewState["ShowLastPage"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the page numbers are displayed.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool ShowPageNumbers
        {
            get
            {
                return ViewState["ShowPageNumbers"] as bool? ?? true;
            }
            set
            {
                ViewState["ShowPageNumbers"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of page numbers to display.
        /// </summary>
        /// <remarks>
        /// The default value is 5.
        /// </remarks>
        [DefaultValue(5)]
        [Category("Appearance")]
        public int PageNumbersCount
        {
            get
            {
                return ViewState["PageNumbersCount"] as int? ?? 5;
            }
            set
            {
                Contract.Requires(value == 0 || value % 2 != 0);

                ViewState["PageNumbersCount"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the page button type to display.
        /// </summary>
        [Category("Appearance")]
        public ButtonType PageButtonType
        {
            get
            {
                return ViewState["PageButtonType"] as ButtonType? ?? ButtonType.Button;
            }
            set
            {
                if(value < ButtonType.Button || value > ButtonType.Link)
                    throw new InvalidEnumArgumentException("value", (int)value, typeof(ButtonType));
                ViewState["PageButtonType"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of an image that is displayed for the first page.
        /// </summary>
        [UrlProperty]
        [Category("Appearance")]
        public string FirstPageImageUrl
        {
            get
            {
                return (string)ViewState["FirstPageImageUrl"];
            }
            set
            {
                ViewState["FirstPageImageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of an image that is displayed for the previous page.
        /// </summary>
        [UrlProperty]
        [Category("Appearance")]
        public string PreviousPageImageUrl
        {
            get
            {
                return (string)ViewState["PreviousPageImageUrl"];
            }
            set
            {
                ViewState["PreviousPageImageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of an image that is displayed for the next page.
        /// </summary>
        [UrlProperty]
        [Category("Appearance")]
        public string NextPageImageUrl
        {
            get
            {
                return (string)ViewState["NextPageImageUrl"];
            }
            set
            {
                ViewState["NextPageImageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of an image that is displayed for the last page.
        /// </summary>
        [UrlProperty]
        [Category("Appearance")]
        public string LastPageImageUrl
        {
            get
            {
                return (string)ViewState["LastPageImageUrl"];
            }
            set
            {
                ViewState["LastPageImageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the URL of an image that is displayed for the page numbers.
        /// </summary>
        [UrlProperty]
        [Category("Appearance")]
        public string PageNumbersImageUrl
        {
            get
            {
                return (string)ViewState["PageNumbersImageUrl"];
            }
            set
            {
                ViewState["PageNumbersImageUrl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the first page.
        /// </summary>
        /// <remarks>
        /// The default value is "≪".
        /// </remarks>
        [DefaultValue("≪")]
        [Localizable(true)]
        [Category("Appearance")]
        public string FirstPageText
        {
            get
            {
                return (string)ViewState["FirstPageText"] ?? @"≪";
            }
            set
            {
                ViewState["FirstPageText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the previous page.
        /// </summary>
        /// <remarks>
        /// The default value is "&lt;".
        /// </remarks>
        [DefaultValue("<")]
        [Localizable(true)]
        [Category("Appearance")]
        public string PreviousPageText
        {
            get
            {
                return (string)ViewState["PreviousPageText"] ?? @"<";
            }
            set
            {
                ViewState["PreviousPageText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the next page.
        /// </summary>
        /// <remarks>
        /// The default value is "&gt;".
        /// </remarks>
        [DefaultValue(">")]
        [Localizable(true)]
        [Category("Appearance")]
        public string NextPageText
        {
            get
            {
                return (string)ViewState["NextPageText"] ?? @">";
            }
            set
            {
                ViewState["NextPageText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the last page.
        /// </summary>
        /// <remarks>
        /// The default value is "≫".
        /// </remarks>
        [DefaultValue("≫")]
        [Localizable(true)]
        [Category("Appearance")]
        public string LastPageText
        {
            get
            {
                return (string)ViewState["LastPageText"] ?? @"≫";
            }
            set
            {
                ViewState["LastPageText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the text that is displayed for the page numbers.
        /// </summary>
        /// <remarks>
        /// The default value is "{0}".
        /// </remarks>
        [DefaultValue("{0}")]
        [Localizable(true)]
        [Category("Appearance")]
        public string PageNumbersText
        {
            get
            {
                return (string)ViewState["PageNumbersText"] ?? @"{0}";
            }
            set
            {
                ViewState["PageNumbersText"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip that is displayed for the first page.
        /// </summary>
        [Localizable(true)]
        [Category("Appearance")]
        public string FirstPageTitle
        {
            get
            {
                return (string)ViewState["FirstPageTitle"] ?? (string)HttpContext.GetLocalResourceObject("~/Pager", "first", CultureInfo.CurrentCulture);
            }
            set
            {
                ViewState["FirstPageTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip that is displayed for the previous page.
        /// </summary>
        [Localizable(true)]
        [Category("Appearance")]
        public string PreviousPageTitle
        {
            get
            {
                return (string)ViewState["PrevousPageTitle"] ?? (string)HttpContext.GetLocalResourceObject("~/Pager", "prev", CultureInfo.CurrentCulture);
            }
            set
            {
                ViewState["PrevousPageTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip that is displayed for the next page.
        /// </summary>
        [Localizable(true)]
        [Category("Appearance")]
        public string NextPageTitle
        {
            get
            {
                return (string)ViewState["NextPageTitle"] ?? (string)HttpContext.GetLocalResourceObject("~/Pager", "next", CultureInfo.CurrentCulture);
            }
            set
            {
                ViewState["NextPageTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip that is displayed for the last page.
        /// </summary>
        [Localizable(true)]
        [Category("Appearance")]
        public string LastPageTitle
        {
            get
            {
                return (string)ViewState["LastPageTitle"] ?? (string)HttpContext.GetLocalResourceObject("~/Pager", "last", CultureInfo.CurrentCulture);
            }
            set
            {
                ViewState["LastPageTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the tool-tip that is displayed for the page numbers.
        /// </summary>
        [Localizable(true)]
        [Category("Appearance")]
        public string PageNumbersTitle
        {
            get
            {
                return (string)ViewState["PageNumbersTitle"] ?? (string)HttpContext.GetLocalResourceObject("~/Pager", "num", CultureInfo.CurrentCulture);
            }
            set
            {
                ViewState["PageNumbersTitle"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether disabled buttons are rendered as <see cref="System.Web.UI.WebControls.Label"/> controls.
        /// </summary>
        /// <remarks>
        /// The default value is <c>true</c>.
        /// </remarks>
        [DefaultValue(true)]
        [Category("Behavior")]
        public bool RenderDisabledButtonsAsLabels
        {
            get
            {
                return ViewState["RenderDisabledButtonsAsLabels"] as bool? ?? true;
            }
            set
            {
                ViewState["RenderDisabledButtonsAsLabels"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether non-breaking spaces will be rendered between controls.
        /// </summary>
        [Category("Behavior")]
        public bool RenderNonBreakingSpacesBetweenControls
        {
            get
            {
                return ViewState["RenderNonBreakingSpacesBetweenControls"] as bool? ?? false;
            }
            set
            {
                ViewState["RenderNonBreakingSpacesBetweenControls"] = value;
            }
        }

        private static void DisposeControls(IEnumerable controls)
        {
            foreach(Control ctrl in controls)
            {
                if(ctrl.HasControls())
                    DisposeControls(ctrl.Controls);
                ctrl.Dispose();
            }
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override void CreateChildControls()
        {
            DisposeControls(Controls);
            Controls.Clear();
            base.CreateChildControls();
            var div = CreateContainer(ShowPages);
            Controls.Add(div);
            InputNumberInteger ctrl = CreateInput(CurrentPage, PageCount, HandlePageEvent);
            div.Controls.Add(ctrl);
            div.Controls.AddAt(div.Controls.Count - 1, new LiteralControl(string.Format(CultureInfo.CurrentCulture, "<label for='{1}'>{0}</label> ", (string)HttpContext.GetLocalResourceObject("~/Pager", "page", CultureInfo.CurrentCulture), ctrl.ClientID)));
            ctrl.CreateValidators();
            div.Controls.Add(new LiteralControl(string.Format(CultureInfo.CurrentCulture, (string)HttpContext.GetLocalResourceObject("~/Pager", "of", CultureInfo.CurrentCulture), PageCount)));
            Controls.Add(new LiteralControl(" "));
            bool first = ShowFirstPage, prev = ShowPreviousPage, next = ShowNextPage, last = ShowLastPage, num = ShowPageNumbers, sep = RenderNonBreakingSpacesBetweenControls;
            div = CreateContainer(first || prev || num || next || last);
            Controls.Add(div);
            int page = CurrentPage, count = PageCount;
            bool show = page == 1, label = RenderDisabledButtonsAsLabels;
            ButtonType button = PageButtonType;
            div.Controls.Add(CreateLiteral(string.Format(CultureInfo.CurrentCulture, "<span title=\"{1}\" class='{2}'>{0}</span>", Page.Server.HtmlEncode(FirstPageText), FirstPageTitle, FirstPageCssClass), first && show && label));
            div.Controls.Add(CreateButton(button, FirstPageText, FirstPageTitle, FirstPageCssClass, "First", FirstPageImageUrl, "start", 1, first, !show, label));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && first && (prev || num || next || last)));
            div.Controls.Add(CreateLiteral(string.Format(CultureInfo.CurrentCulture, "<span title=\"{1}\" class='{2}'>{0}</span>", Page.Server.HtmlEncode(PreviousPageText), PreviousPageTitle, PreviousPageCssClass), prev && show && label));
            div.Controls.Add(CreateButton(button, PreviousPageText, PreviousPageTitle, PreviousPageCssClass, "Previous", PreviousPageImageUrl, "prev", page - 1, prev, !show, label));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && prev && (num || next || last)));
            show = page > 3;
            div.Controls.Add(CreateButton(button, string.Format(CultureInfo.CurrentCulture, PageNumbersText, 1), string.Format(CultureInfo.CurrentCulture, PageNumbersTitle, 1), PageNumbersCssClass, "Page", null, null, 1, num && show, show, label));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && num && show));
            show = page > 4;
            div.Controls.Add(CreateLiteral(" ... ", num && show));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && num && show));
            for(int i = page - (PageNumbersCount / 2), j = page + (PageNumbersCount / 2); i <= j; i++)
            {
                show = page == i;
                div.Controls.Add(CreateLiteral(string.Format(CultureInfo.CurrentCulture, "<span title=\"{1}\" class='{2}'>{0}</span>", Page.Server.HtmlEncode(string.Format(CultureInfo.CurrentCulture, PageNumbersText, i)), string.Format(CultureInfo.CurrentCulture, PageNumbersTitle, i), show ? CurrentPageCssClass : PageNumbersCssClass), num && show && label));
                div.Controls.Add(CreateButton(button, string.Format(CultureInfo.CurrentCulture, PageNumbersText, i), string.Format(CultureInfo.CurrentCulture, PageNumbersTitle, i), show ? CurrentPageCssClass : PageNumbersCssClass, "Page", null, null, i, num && i > 0 && i <= count, !show && i > 0 && i <= count, label));
                div.Controls.Add(CreateLiteral("&nbsp;", sep && num && i > 0 && i < count));
            }
            div.Controls.RemoveAt(div.Controls.Count - 1);
            show = page < count - 3;
            div.Controls.Add(CreateLiteral("&nbsp;", sep && num && show));
            div.Controls.Add(CreateLiteral(" ... ", num && show));
            show = page < count - 2;
            div.Controls.Add(CreateLiteral("&nbsp;", sep && num && show));
            div.Controls.Add(CreateButton(button, string.Format(CultureInfo.CurrentCulture, PageNumbersText, count), string.Format(CultureInfo.CurrentCulture, PageNumbersTitle, count), PageNumbersCssClass, "Page", null, null, count, num & show, show, label));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && next && (num || prev || first)));
            show = page == count;
            div.Controls.Add(CreateLiteral(string.Format(CultureInfo.CurrentCulture, "<span title=\"{1}\" class='{2}'>{0}</span>", Page.Server.HtmlEncode(NextPageText), NextPageTitle, NextPageCssClass), next && show && label));
            div.Controls.Add(CreateButton(button, NextPageText, NextPageTitle, NextPageCssClass, "Next", NextPageImageUrl, "next", page + 1, next, !show, label));
            div.Controls.Add(CreateLiteral("&nbsp;", sep && last && (next || num || prev || first)));
            div.Controls.Add(CreateLiteral(string.Format(CultureInfo.CurrentCulture, "<span title=\"{1}\" class='{2}'>{0}</span>", Page.Server.HtmlEncode(LastPageText), LastPageTitle, LastPageCssClass), last && show && label));
            div.Controls.Add(CreateButton(button, LastPageText, LastPageTitle, LastPageCssClass, "Last", LastPageImageUrl, null, count, last, !show, label));
            Controls.Add(new LiteralControl(" "));
            div = CreateContainer(ShowRows);
            Controls.Add(div);
            ctrl = CreateInput(PageSize, ((BasePage)Page).IsAdmin ? int.MaxValue : Config.PageSize, HandleRowsEvent);
            div.Controls.Add(ctrl);
            div.Controls.AddAt(div.Controls.Count - 1, new LiteralControl(string.Format(CultureInfo.CurrentCulture, "<label for='{1}'>{0}</label> ", (string)HttpContext.GetLocalResourceObject("~/Pager", "rows", CultureInfo.CurrentCulture), ctrl.ClientID)));
            ctrl.CreateValidators();
            div.Controls.Add(new LiteralControl(string.Format(CultureInfo.CurrentCulture, (string)HttpContext.GetLocalResourceObject("~/Pager", "to", CultureInfo.CurrentCulture), TotalRowCount > 0 ? StartRowIndex + 1 : 0, TotalRowCount > 0 ? StartRowIndex + (StartRowIndex + MaximumRows > TotalRowCount ? TotalRowCount - StartRowIndex : MaximumRows) : 0, TotalRowCount > 0 ? TotalRowCount : 0)));
            ChildControlsCreated = true;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static HtmlGenericControl CreateContainer(bool show)
        {
            var div = new HtmlGenericControl("div")
            {
                Visible = show
            };
            div.Attributes["style"] = "display: inline-block; margin: 0 5px 10px; white-space: nowrap;";
            return div;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static LiteralControl CreateLiteral(string text, bool show)
        {
            var ltr = new LiteralControl(text)
            {
                Visible = show
            };
            return ltr;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private InputNumberInteger CreateInput(int value, int max, EventHandler evt)
        {
            var input = new InputNumberInteger
            {
                AutoPostBack = true,
                CausesValidation = true,
                Max = max.ToString(CultureInfo.InvariantCulture),
                Min = "1",
                Required = true,
                ValidationGroup = new[] { ValidationGroup },
                Value = value.ToString(CultureInfo.CurrentCulture)
            };
            input.Style["width"] = "20px";
            input.ServerChange += evt;
            input.ApplyStyleSheetSkin(Page);
            return input;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private Control CreateButton(ButtonType type, string text, string title, string css, string cmd, string src, string rel, int page, bool show, bool enable, bool label)
        {
            switch(type)
            {
                case ButtonType.Button:
                {
                    var button = new InputSubmit
                    {
                        Class = css,
                        CommandArgument = page.ToString(CultureInfo.CurrentCulture),
                        CommandName = cmd,
                        Disabled = !enable,
                        Title = title,
                        ValidationGroup = ValidationGroup,
                        Value = text,
                        Visible = show && (enable || !label)
                    };
                    button.Command += HandleCommandEvent;
                    button.ApplyStyleSheetSkin(Page);
                    return button;
                }
                case ButtonType.Image:
                {
                    var image = new InputImage
                    {
                        Alt = title,
                        Class = css,
                        CommandArgument = page.ToString(CultureInfo.CurrentCulture),
                        CommandName = cmd,
                        Disabled = !enable,
                        Src = !string.IsNullOrWhiteSpace(src) ? src : string.Format(CultureInfo.InvariantCulture, PageNumbersImageUrl, page),
                        Title = title,
                        ValidationGroup = ValidationGroup,
                        Visible = show && (enable || !label)
                    };
                    image.Command += HandleCommandEvent;
                    image.ApplyStyleSheetSkin(Page);
                    return image;
                }
                case ButtonType.Link:
                {
                    var link = new Anchor
                    {
                        Class = enable ? css : DisabledCssClass,
                        HRef = enable ? string.Format(CultureInfo.InvariantCulture, QueryStringNavigateUrl, page) : null,
                        InnerHtml = Page.Server.HtmlEncode(text),
                        Rel = rel,
                        Title = title,
                        Visible = show && (enable || !label)
                    };
                    link.ApplyStyleSheetSkin(Page);
                    return link;
                }
                default:
                    throw new InvalidEnumArgumentException("type", (int)type, typeof(ButtonType));
            }
        }

        private static readonly object EventPagerCommand = new object();

        /// <summary>
        /// Occurs when a button is clicked.
        /// </summary>
        [Category("Action")]
        public event EventHandler<PageEventArgs> PagerCommand
        {
            add
            {
                Events.AddHandler(EventPagerCommand, value);
            }
            remove
            {
                Events.RemoveHandler(EventPagerCommand, value);
            }
        }

        private void OnPagerCommand(PageEventArgs e)
        {
            var handler = Events[EventPagerCommand] as EventHandler<PageEventArgs>;
            if(handler != null)
                handler(this, e);
        }

        private void HandleCommandEvent(object sender, CommandEventArgs e)
        {
            if(!Page.IsValid)
                return;
            StartRowIndex = (int.Parse((string)e.CommandArgument, CultureInfo.CurrentCulture) - 1) * MaximumRows;
            TotalRowCount = TotalRowCount;
            OnPagerCommand(new PageEventArgs(StartRowIndex, MaximumRows, TotalRowCount));
        }

        private void HandlePageEvent(object sender, EventArgs e)
        {
            if(!Page.IsValid)
                return;
            int page = int.Parse(((InputNumberInteger)sender).Value, CultureInfo.CurrentCulture);
            StartRowIndex = (page - 1) * MaximumRows;
            TotalRowCount = TotalRowCount;
            OnPagerCommand(new PageEventArgs(StartRowIndex, MaximumRows, TotalRowCount));
        }

        private void HandleRowsEvent(object sender, EventArgs e)
        {
            if(!Page.IsValid)
                return;
            int rows = int.Parse(((InputNumberInteger)sender).Value, CultureInfo.CurrentCulture);
            StartRowIndex = 0;
            MaximumRows = rows;
            TotalRowCount = TotalRowCount;
            OnPagerCommand(new PageEventArgs(StartRowIndex, MaximumRows, TotalRowCount));
        }
    }
}
