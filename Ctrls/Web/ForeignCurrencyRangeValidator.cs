namespace Ctrls.Web
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Utils;

    /// <summary>
    /// Checks whether the decimal value of an input control is within a specified range of values.
    /// </summary>
    /// <remarks>
    /// Supports custom number of decimal places, client validation, unobtrusive JavaScript, and is fully globalized.
    /// </remarks>
    [ToolboxData("<{0}:ForeignCurrencyRangeValidator runat='server' />")]
    public sealed class ForeignCurrencyRangeValidator : CustomValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForeignCurrencyRangeValidator"/> class.
        /// </summary>
        public ForeignCurrencyRangeValidator()
        {
            ClientValidationFunction = "ForeignCurrencyRange";
            ServerValidate += ForeignCurrencyRange;
        }

        /// <summary>
        /// Gets the data type that the values being compared are converted to before the comparison is made.
        /// </summary>
        [Category("Behavior")]
        public static readonly ValidationDataType Type = ValidationDataType.Currency;

        /// <summary>
        /// Gets or sets a value indicating whether values are converted to a culture-neutral format before being compared.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CultureInvariantValues
        {
            get
            {
                return ViewState["CultureInvariantValues"] as bool? ?? false;
            }
            set
            {
                ViewState["CultureInvariantValues"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum value of the validation range.
        /// </summary>
        /// <remarks>
        /// The default value is "-9007199254740992" (JavaScript compatible).
        /// </remarks>
        [DefaultValue("-9007199254740992")]
        [Themeable(false)]
        [Category("Behavior")]
        public string MinimumValue
        {
            get
            {
                return ViewState["MinimumValue"] as string ?? "-9007199254740992";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal min;
                    if(!decimal.TryParse(value, NumberStyles.Float, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, out min) || min < -9007199254740992 || 9007199254740992 < min)
                        throw new ArgumentOutOfRangeException("value", value, @"-9007199254740992 <= Decimal <= 9007199254740992");
                    if(decimal.Parse(MaximumValue, NumberStyles.Float, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture) < min)
                        throw new ArgumentOutOfRangeException("value", value, @"MinimumValue <= MaximumValue");
                    value = value.Trim();
                }
                else
                    value = null;
                ViewState["MinimumValue"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value of the validation range.
        /// </summary>
        /// <remarks>
        /// The default value is "9007199254740992" (JavaScript compatible).
        /// </remarks>
        [DefaultValue("9007199254740992")]
        [Themeable(false)]
        [Category("Behavior")]
        public string MaximumValue
        {
            get
            {
                return ViewState["MaximumValue"] as string ?? "9007199254740992";
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal max;
                    if(!decimal.TryParse(value, NumberStyles.Float, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, out max) || max < -9007199254740992 || 9007199254740992 < max)
                        throw new ArgumentOutOfRangeException("value", value, @"-9007199254740992 <= Decimal <= 9007199254740992");
                    if(decimal.Parse(MinimumValue, NumberStyles.Float, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture) > max)
                        throw new ArgumentOutOfRangeException("value", value, @"MinimumValue <= MaximumValue");
                    value = value.Trim();
                }
                else
                    value = null;
                ViewState["MaximumValue"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(RenderUplevel)
                ((BasePage)Page).RegisterStartupScript("ForeignCurrencyValidation", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}ForeignCurrencyValidation{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
        }

        /// <inheritdoc/>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            EnsureID();
            base.AddAttributesToRender(writer);
            if(!RenderUplevel)
                return;
            string min = MinimumValue, max = MaximumValue;
            if(CultureInvariantValues)
            {
                min = decimal.Parse(min, CultureInfo.InvariantCulture).ToString("G", CultureInfo.CurrentCulture);
                max = decimal.Parse(max, CultureInfo.InvariantCulture).ToString("G", CultureInfo.CurrentCulture);
            }
            if(writer != null && IsUnobtrusive)
            {
                writer.AddAttribute("data-val-minimumvalue", min, false);
                writer.AddAttribute("data-val-maximumvalue", max, false);
            }
            else
            {
                ScriptManager script = ScriptManager.GetCurrent(Page);
                if(script != null && script.SupportsPartialRendering)
                {
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "minimumvalue", min, false);
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "maximumvalue", max, false);
                }
                else
                {
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "minimumvalue", min, false);
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "maximumvalue", max, false);
                }
            }
        }

        private static void ForeignCurrencyRange(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = false;
            decimal val;
            if(decimal.TryParse(e.Value, NumberStyles.Float, CultureInfo.CurrentCulture, out val))
            {
                var rv = (ForeignCurrencyRangeValidator)sender;
                e.IsValid = decimal.Parse(rv.MinimumValue, rv.CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture) <= val && val <= decimal.Parse(rv.MaximumValue, rv.CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture);
            }
        }
    }
}
