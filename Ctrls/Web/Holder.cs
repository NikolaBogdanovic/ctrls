namespace Ctrls.Web
{
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Stores dynamically added server controls on the Web page, while preserving all whitespace.
    /// </summary>
    [ControlBuilder(typeof(ControlBuilder))]
    [ToolboxData("<{0}:Holder runat='server'></{0}:Holder>")]
    public sealed class Holder : PlaceHolder
    {
    }
}
