namespace Ctrls.Web
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Html;

    /// <summary>
    /// Checks whether the checkbox or a radio button is checked.
    /// </summary>
    /// <remarks>
    /// Supports client validation and unobtrusive JavaScript.
    /// </remarks>
    [ToolboxData("<{0}:CheckedValidator runat='server' />")]
    public sealed class CheckedValidator : CustomValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CheckedValidator"/> class.
        /// </summary>
        public CheckedValidator()
        {
            ClientValidationFunction = "IsChecked";
            ServerValidate += IsChecked;
        }

        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(RenderUplevel)
                ((BasePage)Page).RegisterStartupScript("CheckedValidation", "<script type='text/javascript'>window.IsChecked = function(sender, args) { args.IsValid = asp.$get(sender.controltocompare).checked; };</script>");
        }

        /// <inheritdoc/>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            EnsureID();
            base.AddAttributesToRender(writer);
        }

        private static void IsChecked(object sender, ServerValidateEventArgs e)
        {
            var cv = (CheckedValidator)sender;
            Control ctrl = cv.NamingContainer.FindControl(cv.ControlToValidate);
            var cb = ctrl as InputCheckBox;
            e.IsValid = cb != null ? cb.Checked : ((InputRadio)ctrl).Checked;
        }
    }
}
