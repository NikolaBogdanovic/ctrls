namespace Ctrls.Web
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Html;
    using Utils;

    /// <summary>
    /// Compares the date and time value entered by the user in an input control with the value entered in another input control, or with a constant value.
    /// </summary>
    /// <remarks>
    /// Supports client validation, unobtrusive JavaScript, all <see cref="System.DateTime"/> formats, and is fully globalized and time zone aware.
    /// </remarks>
    [ToolboxData("<{0}:DateTimeCompareValidator runat='server' />")]
    public sealed class DateTimeCompareValidator : CustomValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DateTimeCompareValidator"/> class.
        /// </summary>
        public DateTimeCompareValidator()
        {
            ClientValidationFunction = "DateTimeCompare";
            ServerValidate += DateTimeCompare;
        }

        /// <summary>
        /// Gets the data type that the values being compared are converted to before the comparison is made.
        /// </summary>
        [Category("Behavior")]
        public static readonly Type Type = typeof(DateTime);

        /// <summary>
        /// Gets or sets the comparison operation to perform.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public ValidationCompareOperator Operator
        {
            get
            {
                return ViewState["Operator"] as ValidationCompareOperator? ?? ValidationCompareOperator.Equal;
            }
            set
            {
                if(value < ValidationCompareOperator.Equal || value > ValidationCompareOperator.DataTypeCheck)
                    throw new InvalidEnumArgumentException("value", (int)value, typeof(ValidationCompareOperator));
                ViewState["Operator"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether values are converted to a culture-neutral format before being compared.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CultureInvariantValues
        {
            get
            {
                return ViewState["CultureInvariantValues"] as bool? ?? false;
            }
            set
            {
                ViewState["CultureInvariantValues"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a constant value to compare, in server time zone.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string ValueToCompareInServerTimeZone
        {
            get
            {
                return (string)ViewState["ValueToCompareInServerTimeZone"] ?? string.Empty;
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime dt;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out dt))
                    {
                        if(dt.Kind == DateTimeKind.Utc)
                            dt = G11n.ToServerTimeZone(dt);
                        value = dt.ToString("s", CultureInfo.InvariantCulture);
                    }
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["ValueToCompareInServerTimeZone"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a constant value to compare, in client time zone.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public string ValueToCompare
        {
            get
            {
                return G11n.ToClientTimeZone(DateTime.ParseExact(ValueToCompareInServerTimeZone, "s", CultureInfo.InvariantCulture)).ToString("s", CultureInfo.InvariantCulture);
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    DateTime dt;
                    if(DateTime.TryParse(value, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal, out dt))
                        value = G11n.ToServerTimeZone(dt).ToString("s", CultureInfo.InvariantCulture);
                    else
                        throw new ArgumentOutOfRangeException("value", value, @"DateTime");
                }
                else
                    value = null;
                ViewState["ValueToCompareInServerTimeZone"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the input control to compare with the input control being validated.
        /// </summary>
        [TypeConverter(typeof(ValidatedControlConverter))]
        [IDReferenceProperty]
        [Themeable(false)]
        [Category("Behavior")]
        public string ControlToCompare
        {
            get
            {
                return (string)ViewState["ControlToCompare"] ?? string.Empty;
            }
            set
            {
                ViewState["ControlToCompare"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(!RenderUplevel)
                return;
            var page = (BasePage)Page;
            if(!page.IsStartupScriptRegistered("DateTimePatterns"))
            {
                var custom = new List<string>(132);
                var standard = new[] { 'd', 'g', 's', 'G', 'D', 'f', 'F' };
                for(int i = 0, j = standard.Length; i < j; i++)
                    custom.AddRange(CultureInfo.CurrentCulture.DateTimeFormat.GetAllDateTimePatterns(standard[i]));
                page.RegisterStartupScript("DateTimePatterns", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript'>var DateTimePatterns = [\"{0}\"];</script>", string.Join("\", \"", custom)));
            }
            page.RegisterStartupScript("DateTimeValidation", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}DateTimeValidation{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
        }

        /// <inheritdoc/>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            EnsureID();
            base.AddAttributesToRender(writer);
            if(!RenderUplevel)
                return;
            if(writer != null && IsUnobtrusive)
            {
                writer.AddAttribute("data-val-operator", Operator.GetName(), false);
                if(!string.IsNullOrWhiteSpace(ControlToCompare))
                {
                    writer.AddAttribute("data-val-controltocompare", GetControlRenderID(ControlToCompare), false);
                    writer.AddAttribute("data-val-controlhookup", GetControlRenderID(ControlToCompare), false);
                }
                else if(!string.IsNullOrWhiteSpace(ValueToCompare))
                    writer.AddAttribute("data-val-valuetocompare", ValueToCompare, false);
            }
            else
            {
                ScriptManager script = ScriptManager.GetCurrent(Page);
                if(script != null && script.SupportsPartialRendering)
                {
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "operator", Operator.GetName(), false);
                    if(!string.IsNullOrWhiteSpace(ControlToCompare))
                    {
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "controltocompare", GetControlRenderID(ControlToCompare), false);
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "controlhookup", GetControlRenderID(ControlToCompare), false);
                    }
                    else if(!string.IsNullOrWhiteSpace(ValueToCompare))
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "valuetocompare", ValueToCompare, false);
                }
                else
                {
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "operator", Operator.GetName(), false);
                    if(!string.IsNullOrWhiteSpace(ControlToCompare))
                    {
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "controltocompare", GetControlRenderID(ControlToCompare), false);
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "controlhookup", GetControlRenderID(ControlToCompare), false);
                    }
                    else if(!string.IsNullOrWhiteSpace(ValueToCompare))
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "valuetocompare", ValueToCompare, false);
                }
            }
        }

        private static void DateTimeCompare(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = false;
            DateTime val1;
            if(DateTime.TryParse(e.Value, CultureInfo.CurrentCulture, DateTimeStyles.None, out val1))
            {
                var cv = (DateTimeCompareValidator)sender;
                if(cv.Operator == ValidationCompareOperator.DataTypeCheck)
                {
                    e.IsValid = true;
                    return;
                }
                DateTime val2;
                if(!string.IsNullOrWhiteSpace(cv.ControlToCompare))
                {
                    if(!DateTime.TryParse(((InputDateTimeLocal)cv.NamingContainer.FindControl(cv.ControlToCompare)).Value, CultureInfo.CurrentCulture, DateTimeStyles.None, out val2))
                    {
                        e.IsValid = true;
                        return;
                    }
                }
                else if(!string.IsNullOrWhiteSpace(cv.ValueToCompare))
                    val2 = DateTime.ParseExact(cv.ValueToCompare, "s", CultureInfo.InvariantCulture);
                else
                    return;
                switch(cv.Operator)
                {
                    case ValidationCompareOperator.Equal:
                    {
                        e.IsValid = val1 == val2;
                        return;
                    }
                    case ValidationCompareOperator.NotEqual:
                    {
                        e.IsValid = val1 != val2;
                        return;
                    }
                    case ValidationCompareOperator.LessThan:
                    {
                        e.IsValid = val1 < val2;
                        return;
                    }
                    case ValidationCompareOperator.GreaterThan:
                    {
                        e.IsValid = val1 > val2;
                        return;
                    }
                    case ValidationCompareOperator.LessThanEqual:
                    {
                        e.IsValid = val1 <= val2;
                        return;
                    }
                    case ValidationCompareOperator.GreaterThanEqual:
                    {
                        e.IsValid = val1 >= val2;
                        return;
                    }
                    default:
                        throw new InvalidEnumArgumentException(null, (int)cv.Operator, typeof(ValidationCompareOperator));
                }
            }
        }
    }
}
