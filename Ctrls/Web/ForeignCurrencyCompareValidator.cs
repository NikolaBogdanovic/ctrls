namespace Ctrls.Web
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Ctrls.Handlers;
    using Ctrls.Html;
    using Utils;

    /// <summary>
    /// <see cref="CompareValidator"/> extension methods.
    /// </summary>
    public static class CompareValidatorExtensions
    {
        /// <summary>
        /// Gets the hard-coded <see cref="ValidationCompareOperator"/> names, to avoid reflection.
        /// </summary>
        /// <param name="compare">The validation compare operator.</param>
        /// <returns>
        /// The name of a validation compare operator.
        /// </returns>
        public static string GetName(this ValidationCompareOperator compare)
        {
            switch(compare)
            {
                case ValidationCompareOperator.Equal:
                    return "Equal";
                case ValidationCompareOperator.NotEqual:
                    return "NotEqual";
                case ValidationCompareOperator.GreaterThan:
                    return "GreaterThan";
                case ValidationCompareOperator.GreaterThanEqual:
                    return "GreaterThanEqual";
                case ValidationCompareOperator.LessThan:
                    return "LessThan";
                case ValidationCompareOperator.LessThanEqual:
                    return "LessThanEqual";
                case ValidationCompareOperator.DataTypeCheck:
                    return "DataTypeCheck";
                default:
                    throw new InvalidEnumArgumentException("compare", (int)compare, typeof(ValidationCompareOperator));
            }
        }

        /// <summary>
        /// Gets the hard-coded <see cref="ValidationDataType"/> names, to avoid reflection.
        /// </summary>
        /// <param name="type">The validation data type.</param>
        /// <returns>
        /// The name of a validation data type.
        /// </returns>
        public static string GetName(this ValidationDataType type)
        {
            switch(type)
            {
                case ValidationDataType.String:
                    return "String";
                case ValidationDataType.Integer:
                    return "Integer";
                case ValidationDataType.Double:
                    return "Double";
                case ValidationDataType.Date:
                    return "Date";
                case ValidationDataType.Currency:
                    return "Currency";
                default:
                    throw new InvalidEnumArgumentException("type", (int)type, typeof(ValidationDataType));
            }
        }
    }

    /// <summary>
    /// Compares the decimal value entered by the user in an input control with the value entered in another input control, or with a constant value.
    /// </summary>
    /// <remarks>
    /// Supports custom number of decimal places, client validation, unobtrusive JavaScript, and is fully globalized.
    /// </remarks>
    [ToolboxData("<{0}:ForeignCurrencyCompareValidator runat='server' />")]
    public sealed class ForeignCurrencyCompareValidator : CustomValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ForeignCurrencyCompareValidator"/> class.
        /// </summary>
        public ForeignCurrencyCompareValidator()
        {
            ClientValidationFunction = "ForeignCurrencyCompare";
            ServerValidate += ForeignCurrencyCompare;
        }

        /// <summary>
        /// Gets the data type that the values being compared are converted to before the comparison is made.
        /// </summary>
        [Category("Behavior")]
        public static readonly ValidationDataType Type = ValidationDataType.Currency;

        /// <summary>
        /// Gets or sets the comparison operation to perform.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public ValidationCompareOperator Operator
        {
            get
            {
                return ViewState["Operator"] as ValidationCompareOperator? ?? ValidationCompareOperator.Equal;
            }
            set
            {
                if(value < ValidationCompareOperator.Equal || value > ValidationCompareOperator.DataTypeCheck)
                    throw new InvalidEnumArgumentException("value", (int)value, typeof(ValidationCompareOperator));
                ViewState["Operator"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether values are converted to a culture-neutral format before being compared.
        /// </summary>
        [Themeable(false)]
        [Category("Behavior")]
        public bool CultureInvariantValues
        {
            get
            {
                return ViewState["CultureInvariantValues"] as bool? ?? false;
            }
            set
            {
                ViewState["CultureInvariantValues"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a constant value to compare.
        /// </summary>
        /// <remarks>
        /// Must be between "-9007199254740992" and "9007199254740992" (JavaScript compatible).
        /// </remarks>
        [Themeable(false)]
        [Category("Behavior")]
        public string ValueToCompare
        {
            get
            {
                return (string)ViewState["ValueToCompare"] ?? string.Empty;
            }
            set
            {
                if(!string.IsNullOrWhiteSpace(value))
                {
                    decimal m;
                    if(!decimal.TryParse(value, NumberStyles.Float, CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture, out m) || m < -9007199254740992 || 9007199254740992 < m)
                        throw new ArgumentOutOfRangeException("value", value, @"-9007199254740992 <= Decimal <= 9007199254740992");
                    value = value.Trim();
                }
                else
                    value = null;
                ViewState["ValueToCompare"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the input control to compare with the input control being validated.
        /// </summary>
        [TypeConverter(typeof(ValidatedControlConverter))]
        [IDReferenceProperty]
        [Themeable(false)]
        [Category("Behavior")]
        public string ControlToCompare
        {
            get
            {
                return (string)ViewState["ControlToCompare"] ?? string.Empty;
            }
            set
            {
                ViewState["ControlToCompare"] = value;
            }
        }

        /// <inheritdoc/>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if(RenderUplevel)
                ((BasePage)Page).RegisterStartupScript("ForeignCurrencyValidation", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}ForeignCurrencyValidation{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
        }

        /// <inheritdoc/>
        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            EnsureID();
            base.AddAttributesToRender(writer);
            if(!RenderUplevel)
                return;
            string value = ValueToCompare;
            if(CultureInvariantValues && !string.IsNullOrWhiteSpace(value))
                value = decimal.Parse(value, NumberStyles.Float, CultureInfo.InvariantCulture).ToString("G", CultureInfo.CurrentCulture);
            if(writer != null && IsUnobtrusive)
            {
                writer.AddAttribute("data-val-operator", Operator.GetName(), false);
                if(!string.IsNullOrWhiteSpace(ControlToCompare))
                {
                    writer.AddAttribute("data-val-controltocompare", GetControlRenderID(ControlToCompare), false);
                    writer.AddAttribute("data-val-controlhookup", GetControlRenderID(ControlToCompare), false);
                }
                else if(!string.IsNullOrWhiteSpace(value))
                    writer.AddAttribute("data-val-valuetocompare", value, false);
            }
            else
            {
                ScriptManager script = ScriptManager.GetCurrent(Page);
                if(script != null && script.SupportsPartialRendering)
                {
                    ScriptManager.RegisterExpandoAttribute(this, ClientID, "operator", Operator.GetName(), false);
                    if(!string.IsNullOrWhiteSpace(ControlToCompare))
                    {
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "controltocompare", GetControlRenderID(ControlToCompare), false);
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "controlhookup", GetControlRenderID(ControlToCompare), false);
                    }
                    else if(!string.IsNullOrWhiteSpace(value))
                        ScriptManager.RegisterExpandoAttribute(this, ClientID, "valuetocompare", value, false);
                }
                else
                {
                    Page.ClientScript.RegisterExpandoAttribute(ClientID, "operator", Operator.GetName(), false);
                    if(!string.IsNullOrWhiteSpace(ControlToCompare))
                    {
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "controltocompare", GetControlRenderID(ControlToCompare), false);
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "controlhookup", GetControlRenderID(ControlToCompare), false);
                    }
                    else if(!string.IsNullOrWhiteSpace(value))
                        Page.ClientScript.RegisterExpandoAttribute(ClientID, "valuetocompare", value, false);
                }
            }
        }

        private static void ForeignCurrencyCompare(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = false;
            decimal val1;
            if(decimal.TryParse(e.Value, NumberStyles.Float, CultureInfo.CurrentCulture, out val1))
            {
                var cv = (ForeignCurrencyCompareValidator)sender;
                if(cv.Operator == ValidationCompareOperator.DataTypeCheck)
                {
                    e.IsValid = true;
                    return;
                }
                decimal val2;
                if(!string.IsNullOrWhiteSpace(cv.ControlToCompare))
                {
                    if(!decimal.TryParse(((InputNumberCurrency)cv.NamingContainer.FindControl(cv.ControlToCompare)).Value, NumberStyles.Float, CultureInfo.CurrentCulture, out val2))
                    {
                        e.IsValid = true;
                        return;
                    }
                }
                else if(!string.IsNullOrWhiteSpace(cv.ValueToCompare))
                    val2 = decimal.Parse(cv.ValueToCompare, NumberStyles.Float, cv.CultureInvariantValues ? CultureInfo.InvariantCulture : CultureInfo.CurrentCulture);
                else
                    return;
                switch(cv.Operator)
                {
                    case ValidationCompareOperator.Equal:
                    {
                        e.IsValid = val1 == val2;
                        return;
                    }
                    case ValidationCompareOperator.NotEqual:
                    {
                        e.IsValid = val1 != val2;
                        return;
                    }
                    case ValidationCompareOperator.LessThan:
                    {
                        e.IsValid = val1 < val2;
                        return;
                    }
                    case ValidationCompareOperator.GreaterThan:
                    {
                        e.IsValid = val1 > val2;
                        return;
                    }
                    case ValidationCompareOperator.LessThanEqual:
                    {
                        e.IsValid = val1 <= val2;
                        return;
                    }
                    case ValidationCompareOperator.GreaterThanEqual:
                    {
                        e.IsValid = val1 >= val2;
                        return;
                    }
                    default:
                        throw new InvalidEnumArgumentException(null, (int)cv.Operator, typeof(ValidationCompareOperator));
                }
            }
        }
    }
}
