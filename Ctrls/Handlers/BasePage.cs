﻿namespace Ctrls.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    using Ctrls.Html;
    using Ctrls.Modules;
    using Ctrls.Web;
    using Utils;
    using Utils.Types;

    /// <summary>
    /// Base content page.
    /// </summary>
    /// <remarks>
    /// Prevents one-click attacks.
    /// <para></para>
    /// Initializes the master page and all of its controls, to be accessible in the <see cref="System.Web.UI.Page.PreInit"/> event.
    /// <para></para>
    /// Compresses the <see cref="PageStatePersister.ControlState"/> and <see cref="PageStatePersister.ViewState"/> before encryption, and decompresses the "__VIEWSTATE" after decryption. Compression of cleartext is significantly better than of cyphertext, and cryptography is much faster with smaller data.
    /// <para></para>
    /// Cancels the postback if user roles changed.
    /// <para></para>
    /// Avoids duplicate HTML &lt;SCRIPT&gt; tags on asynchronous postbacks, and re-registers original scripts on full postbacks.
    /// </remarks>
    public class BasePage : Page
    {
        private readonly List<BaseControl> _ctrls = new List<BaseControl>(0);
        private readonly List<IDynamicValidation> _vals = new List<IDynamicValidation>(0);
        private readonly List<KeyValuePair<string, string>> _blocks = new List<KeyValuePair<string, string>>(0);
        private readonly List<KeyValuePair<string, string>> _startups = new List<KeyValuePair<string, string>>(0);

        private bool _end;
        private HtmlLink _canonical;

        /// <summary>
        /// Gets a value indicating whether the current user is in the Admins role.
        /// </summary>
        public bool IsAdmin { get; private set; }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override void OnPreInit(EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
            IsAdmin = User.IsInRole("Admins");
            RegisterStartupScript("Default", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}Default{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
            var master = Master as BaseMaster;
            if(master != null)
                master.OnPreInit(e);
            foreach(BaseControl c in _ctrls)
                c.OnPreInit(e);
            base.OnPreInit(e);
        }

        /// <inheritdoc/>
        protected override void OnInit(EventArgs e)
        {
            if(_canonical != null && _canonical.Page == null)
                Header.Controls.Add(_canonical);
            foreach(IDynamicValidation v in _vals)
                v.CreateValidators();
            base.OnInit(e);
        }

        /// <inheritdoc/>
        protected override object LoadPageStateFromPersistenceMedium()
        {
            PageStatePersister.Load();
            if(PageStatePersister.ControlState != null)
                PageStatePersister.ControlState = new LosFormatter().Deserialize(Crypto.DeflateDecompress(PageStatePersister.ControlState.ToString(), false));
            if(PageStatePersister.ViewState != null)
                PageStatePersister.ViewState = new LosFormatter().Deserialize(Crypto.DeflateDecompress(PageStatePersister.ViewState.ToString(), false));
            return new Pair(PageStatePersister.ControlState, PageStatePersister.ViewState);
        }

        /// <inheritdoc/>
        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            var old = (string[])ViewState["Roles"];
            var now = Roles.Enabled ? Roles.GetRolesForUser() : CookieRoles.GetRolesForUser();
            if(old.Length == now.Length)
            {
                Array.Sort(old);
                Array.Sort(now);
                for(int i = 0, j = old.Length; i < j; i++)
                {
                    if(old[i] != now[i])
                    {
                        RequestReload();
                        return;
                    }
                }
            }
            else
                RequestReload();
        }

        /// <inheritdoc/>
        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            if(!_end)
                base.RaisePostBackEvent(sourceControl, eventArgument);
        }

        /// <inheritdoc/>
        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);
            Title = string.Format(CultureInfo.CurrentCulture, "{0} - {1}", Title, Config.WebsiteName);
            if(Page.Validators.Count > 0)
            {
                RegisterStartupScript("jQuery-UI", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='http{0}://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui{1}.js' onerror=\"this.onerror = null; this.src = '{2}jQuery/jquery-ui{1}.js';\"></script>", Request.IsSecureConnection ? "s" : null, !Context.IsDebuggingEnabled ? ".min" : null, Properties.Settings.Default.ScriptsPath));
                RegisterStartupScript("ValidatorCallout", string.Format(CultureInfo.InvariantCulture, "<script type='text/javascript' src='{0}ValidatorCallout{1}.js{2}' onerror=\"this.onerror = null; this.src = this.src.slice(this.src.indexOf('{3}'));\"></script>", Link.Static(Properties.Settings.Default.ScriptsPath, Page.Request.IsSecureConnection), !Context.IsDebuggingEnabled ? ".min" : null, Config.CacheControl, Properties.Settings.Default.ScriptsPath));
            }
            var async = ScriptManager.GetCurrent(Page);
            if(async != null && async.SupportsPartialRendering)
            {
                var type = typeof(Page);
                foreach(KeyValuePair<string, string> kvp in _blocks)
                {
                    if(IsPostBack && !async.IsInAsyncPostBack)
                    {
                        ViewState[kvp.Key] = null;
                        foreach(RegisteredScript script in async.GetRegisteredClientScriptBlocks())
                        {
                            if(script.Type == type && script.Key == kvp.Key)
                            {
                                ViewState[kvp.Key] = kvp.Key;
                                break;
                            }
                        }
                    }
                    if(ViewState[kvp.Key] == null)
                    {
                        ViewState[kvp.Key] = kvp.Key;
                        ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), kvp.Key, kvp.Value, false);
                    }
                }
                foreach(KeyValuePair<string, string> kvp in _startups)
                {
                    if(IsPostBack && !async.IsInAsyncPostBack)
                    {
                        ViewState[kvp.Key] = null;
                        foreach(RegisteredScript script in async.GetRegisteredStartupScripts())
                        {
                            if(script.Type == type && script.Key == kvp.Key)
                            {
                                ViewState[kvp.Key] = kvp.Key;
                                break;
                            }
                        }
                    }
                    if(ViewState[kvp.Key] == null)
                    {
                        ViewState[kvp.Key] = true;
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), kvp.Key, kvp.Value, false);
                    }
                }
            }
            else
            {
                foreach(KeyValuePair<string, string> kvp in _blocks)
                {
                    if(!Page.ClientScript.IsClientScriptBlockRegistered(typeof(Page), kvp.Key))
                        Page.ClientScript.RegisterClientScriptBlock(typeof(Page), kvp.Key, kvp.Value, false);
                }
                foreach(KeyValuePair<string, string> kvp in _startups)
                {
                    if(!Page.ClientScript.IsStartupScriptRegistered(typeof(Page), kvp.Key))
                        Page.ClientScript.RegisterStartupScript(typeof(Page), kvp.Key, kvp.Value, false);
                }
            }
        }

        /// <inheritdoc/>
        protected override object SaveViewState()
        {
            if(_end)
                return null;
            if(!IsPostBack)
                ViewState["Roles"] = Roles.Enabled ? Roles.GetRolesForUser() : CookieRoles.GetRolesForUser();
            return base.SaveViewState();
        }

        /// <inheritdoc/>
        protected override void SavePageStateToPersistenceMedium(object state)
        {
            if(_end)
                return;
            var pair = state as Pair;
            if(pair != null)
            {
                PageStatePersister.ControlState = pair.First;
                PageStatePersister.ViewState = pair.Second;
            }
            else
                PageStatePersister.ViewState = state;
            var sb = new StringBuilder();
            if(PageStatePersister.ControlState != null)
            {
                using(var writer = new SpecifiedEncodingStringWriter(sb, Encoding.UTF8))
                    new LosFormatter().Serialize(writer, PageStatePersister.ControlState);
                PageStatePersister.ControlState = Crypto.DeflateCompress(sb.ToString(), false);
            }
            sb.Clear();
            if(PageStatePersister.ViewState != null)
            {
                using(var writer = new SpecifiedEncodingStringWriter(sb, Encoding.UTF8))
                    new LosFormatter().Serialize(writer, PageStatePersister.ViewState);
                PageStatePersister.ViewState = Crypto.DeflateCompress(sb.ToString(), false);
            }
            PageStatePersister.Save();
        }

        /// <inheritdoc/>
        public override void RenderControl(HtmlTextWriter writer)
        {
            if(!_end)
                base.RenderControl(writer);
        }

        /// <inheritdoc/>
        protected override void Render(HtmlTextWriter writer)
        {
            if(!_end)
                base.Render(writer);
        }

        /// <inheritdoc/>
        protected override void RenderChildren(HtmlTextWriter writer)
        {
            if(!_end)
                base.RenderChildren(writer);
        }

        /// <summary>
        /// Gets or sets the href of the "canonical" link element.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public string LinkCanonical
        {
            get
            {
                if(_canonical != null)
                    return _canonical.Href;
                return null;
            }
            set
            {
                if(_canonical == null)
                {
                    if(!string.IsNullOrWhiteSpace(value))
                    {
                        _canonical = new HtmlLink
                        {
                            Href = value
                        };
                        _canonical.Attributes["rel"] = "canonical";
                        if(Header != null)
                            Header.Controls.Add(_canonical);
                    }
                }
                else if(string.IsNullOrWhiteSpace(value))
                {
                    if(Header != null)
                        Header.Controls.Remove(_canonical);
                    _canonical.Dispose();
                    _canonical = null;
                }
                else
                    _canonical.Href = value;
            }
        }

        /// <summary>
        /// Registers a postback control as an asynchronous trigger.
        /// </summary>
        /// <param name="sender">The postback control.</param>
        /// <param name="e">The event data.</param>
        public void AsyncTrigger(object sender, EventArgs e)
        {
            var script = ScriptManager.GetCurrent(Page);
            if(script != null)
                script.RegisterAsyncPostBackControl((Control)sender);
        }

        /// <summary>
        /// Initializes a validator control.
        /// </summary>
        /// <param name="sender">The validator control.</param>
        /// <param name="e">The event data.</param>
        public void BaseValidator(object sender, EventArgs e)
        {
            var bv = (BaseValidator)sender;
            if(bv.ID == null)
                bv.ID = bv.UniqueID.Substring(bv.NamingContainer.UniqueID.Length + 1);
            bv.Display = ValidatorDisplay.Dynamic;
            bv.SetFocusOnError = true;
            bv.CssClass = "Validator";
            if(string.IsNullOrWhiteSpace(bv.ErrorMessage))
            {
                if(sender is RequiredFieldValidator)
                    bv.ErrorMessage = (string)HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, "required", CultureInfo.CurrentCulture);
                else
                {
                    string resx = null;
                    var cv = sender as CompareValidator;
                    if(cv != null)
                        resx = cv.Operator == ValidationCompareOperator.DataTypeCheck ? cv.Type.GetName() : cv.Operator.GetName();
                    else
                    {
                        var fc = sender as ForeignCurrencyCompareValidator;
                        if(fc != null)
                            resx = fc.Operator == ValidationCompareOperator.DataTypeCheck ? DateTimeCompareValidator.Type.Name : fc.Operator.GetName();
                        else
                        {
                            var dt = sender as DateTimeCompareValidator;
                            if(dt != null)
                                resx = dt.Operator == ValidationCompareOperator.DataTypeCheck ? DateTimeCompareValidator.Type.Name : dt.Operator.GetName();
                        }
                    }
                    if(resx != null)
                        bv.ErrorMessage = (string)HttpContext.GetLocalResourceObject(Properties.Settings.Default.ValidatorsResx, resx, CultureInfo.CurrentCulture);
                }
            }
        }

        /// <summary>
        /// Register a user control for <see cref="OnPreInit"/> event.
        /// </summary>
        /// <param name="ctrl">The user control.</param>
        public void UserControl(BaseControl ctrl)
        {
            Contract.Requires(ctrl != null);

            _ctrls.Add(ctrl);
        }

        /// <summary>
        /// Register a form control for dynamic validation.
        /// </summary>
        /// <param name="sender">The form control.</param>
        /// <param name="e">The event data.</param>
        public void DynamicValidation(object sender, EventArgs e)
        {
            _vals.Add((IDynamicValidation)sender);
        }

        /// <summary>
        /// Escapes special characters from a control identifier.
        /// </summary>
        /// <param name="id">The control identifier.</param>
        /// <returns>
        /// An escaped control identifier.
        /// </returns>
        public static string EscapeID(string id)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(id));

            var sb = new StringBuilder();
            foreach(char c in id)
                sb.Append(char.IsLetterOrDigit(c) ? c : '_');
            return sb.ToString();
        }

        /// <summary>
        /// Resets form controls.
        /// </summary>
        /// <param name="ctrls">The form controls.</param>
        public static void ResetControls(ControlCollection ctrls)
        {
            Contract.Requires(ctrls != null);

            foreach(Control c in ctrls)
            {
                var cb = c as InputCheckBox;
                if(cb != null)
                {
                    if(!cb.Disabled)
                        cb.Checked = false;
                    continue;
                }
                var rb = c as InputRadio;
                if(rb != null)
                {
                    if(!rb.Disabled)
                        rb.Checked = false;
                    continue;
                }
                var txt = c as InputText;
                if(txt != null)
                {
                    if(!txt.Disabled)
                        txt.Value = null;
                    continue;
                }
                var ta = c as TextArea;
                if(ta != null)
                {
                    if(!ta.Disabled)
                        ta.Value = null;
                    continue;
                }
                var sel = c as Select;
                if(sel != null)
                {
                    if(!sel.Disabled)
                        sel.SelectedIndex = -1;
                }
                else if(c.HasControls())
                    ResetControls(c.Controls);
            }
        }

        /// <summary>
        /// Terminates execution of the current request.
        /// </summary>
        public void RequestEnd()
        {
            _end = true;
            Context.ApplicationInstance.CompleteRequest();
        }

        /// <summary>
        /// Terminates execution of the current request, and performs an asynchronous execution of the raw URL.
        /// </summary>
        public void RequestReload()
        {
            Response.Clear();
            var script = ScriptManager.GetCurrent(Page);
            if(script != null && script.IsInAsyncPostBack)
            {
                Response.Redirect(Link.Absolute(Request.RawUrl, Request.IsSecureConnection), false);
                RequestEnd();
            }
            else
                Server.TransferRequest(Request.RawUrl, false);
        }

        /// <summary>
        /// Terminates execution of the current request, and redirects request to a new URL.
        /// </summary>
        /// <param name="url">The new URL.</param>
        public void RequestRedirect(string url)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(url));

            Response.Clear();
            Response.Redirect(url, false);
            RequestEnd();
        }

        /// <summary>
        /// Determines whether the client script block with the specified key is registered with the page.
        /// </summary>
        /// <param name="key">The key of the client script to search for.</param>
        /// <returns>
        /// <c>true</c> if the script block is registered; otherwise, <c>false</c>.
        /// </returns>
        public new bool IsClientScriptBlockRegistered(string key)
        {
            return _blocks.Exists(kvp => kvp.Key == key);
        }

        /// <summary>
        /// Determines whether the client startup script is registered with the page.
        /// </summary>
        /// <param name="key">The key of the startup script to search for.</param>
        /// <returns>
        /// <c>true</c> if the startup script is registered; otherwise, <c>false</c>.
        /// </returns>
        public new bool IsStartupScriptRegistered(string key)
        {
            return _startups.Exists(kvp => kvp.Key == key);
        }

        /// <summary>
        /// Emits client-side script blocks in the page response.
        /// </summary>
        /// <param name="key">Unique key that identifies a script block.</param>
        /// <param name="script">Content of script that is sent to the client.</param>
        public new void RegisterClientScriptBlock(string key, string script)
        {
            if(!IsClientScriptBlockRegistered(key))
                _blocks.Add(new KeyValuePair<string, string>(key, script));
        }

        /// <summary>
        /// Emits a client-side startup script in the page response.
        /// </summary>
        /// <param name="key">Unique key that identifies a startup script.</param>
        /// <param name="script">Content of script that is sent to the client.</param>
        public new void RegisterStartupScript(string key, string script)
        {
            if(!IsStartupScriptRegistered(key))
                _startups.Add(new KeyValuePair<string, string>(key, script));
        }
    }
}
