﻿namespace Ctrls.Handlers
{
    using System.Web;

    using Utils;

    /// <summary>
    /// Handles file downloading using the "name" and "path" query strings.
    /// </summary>
    public sealed class Download : IHttpHandler
    {
        /// <inheritdoc/>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <inheritdoc/>
        public void ProcessRequest(HttpContext context)
        {
            if(!string.IsNullOrWhiteSpace(context.Request.QueryString["name"]) && !string.IsNullOrWhiteSpace(context.Request.QueryString["path"]))
            {
                context.Response.ContentType = "application/octet-stream";
                context.Response.AddHeader("Content-Disposition", "attachment;filename=" + context.Request.QueryString["name"]);
                context.Response.TransmitFile(context.Request.QueryString["path"]);
            }
            else
            {
                context.Response.ContentType = ContentType.TEXT.GetName();
                context.Response.Write("No input file specified! Use the \"name\" and \"path\" query strings.");
            }
        }
    }
}
