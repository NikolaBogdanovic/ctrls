﻿namespace Ctrls.Handlers
{
    using System;
    using System.Web.UI;

    /// <summary>
    /// Base master page.
    /// </summary>
    public class BaseMaster : MasterPage
    {
        /// <summary>
        /// Gets a value indicating whether the current user is in the Admins role.
        /// </summary>
        protected bool IsAdmin
        {
            get
            {
                return ((BasePage)Page).IsAdmin;
            }
        }

        private static readonly object EventPreInit = new object();

        /// <summary>
        /// Occurs at the beginning of master page initialization.
        /// </summary>
        /// <remarks>
        /// Occurs before the <see cref="PageStatePersister.ViewState"/> is tracking changes for master page child controls.
        /// <para></para>
        /// Initializes the nested master pages and all of their child controls, to be accessible in the <see cref="System.Web.UI.Page.PreInit"/> event.
        /// </remarks>
        public event EventHandler PreInit
        {
            add
            {
                Events.AddHandler(EventPreInit, value);
            }
            remove
            {
                Events.RemoveHandler(EventPreInit, value);
            }
        }

        /// <summary>
        /// Raises the <see cref="PreInit"/> event at the beginning of master page initialization.
        /// </summary>
        /// <param name="e">The event data.</param>
        public virtual void OnPreInit(EventArgs e)
        {
            var master = Master as BaseMaster;
            if(master != null)
                master.OnPreInit(e);
            var handler = Events[EventPreInit] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Registers a postback control as an asynchronous trigger.
        /// </summary>
        /// <param name="sender">The postback control.</param>
        /// <param name="e">The event data.</param>
        protected void AsyncTrigger(object sender, EventArgs e)
        {
            ((BasePage)Page).AsyncTrigger(sender, e);
        }

        /// <summary>
        /// Initializes a validator control.
        /// </summary>
        /// <param name="sender">The validator control.</param>
        /// <param name="e">The event data.</param>
        public void BaseValidator(object sender, EventArgs e)
        {
            ((BasePage)Page).BaseValidator(sender, e);
        }

        /// <summary>
        /// Register a form control for dynamic validation.
        /// </summary>
        /// <param name="sender">The form control.</param>
        /// <param name="e">The event data.</param>
        protected void DynamicValidation(object sender, EventArgs e)
        {
            ((BasePage)Page).DynamicValidation(sender, e);
        }
    }
}
