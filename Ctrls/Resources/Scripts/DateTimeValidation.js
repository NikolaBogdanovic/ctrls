﻿(function(asp)
{
    'use strict';
    
    window.DateTimeCompare = function(sender, args)
    {
        args.IsValid = false;
        var val1 = Date.parseLocale.apply(null, [args.Value].concat(asp.DateTimePatterns));
        if(val1)
        {
            if(sender.operator === 'DataTypeCheck')
            {
                args.IsValid = true;
                return;
            }
            var val2;
            if(sender.controltocompare)
            {
                val2 = Date.parseLocale.apply(null, [asp.$get(sender.controltocompare).value].concat(asp.DateTimePatterns));
                if(!val2)
                {
                    args.IsValid = true;
                    return;
                }
            }
            else if(sender.valuetocompare)
                val2 = Date.parseLocale.apply(null, [sender.valuetocompare].concat(asp.DateTimePatterns));
            else
                return;
            switch(sender.operator)
            {
                case 'Equal':
                    args.IsValid = val1 === val2;
                    break;
                case 'NotEqual':
                    args.IsValid = val1 !== val2;
                    break;
                case 'LessThan':
                    args.IsValid = val1 < val2;
                    break;
                case 'GreaterThan':
                    args.IsValid = val1 > val2;
                    break;
                case 'LessThanEqual':
                    args.IsValid = val1 <= val2;
                    break;
                case 'GreaterThanEqual':
                    args.IsValid = val1 >= val2;
                    break;
                default:
                    throw new Error('Unknown Validation Compare Operator!');
            }
        }
    };

    window.DateTimeRange = function(sender, args)
    {
        args.IsValid = false;
        var val = Date.parseLocale.apply(null, [args.Value].concat(asp.DateTimePatterns));
        if(val)
            args.IsValid = Date.parseLocale.apply(null, [sender.minimumvalue].concat(asp.DateTimePatterns)) <= val && val <= Date.parseLocale.apply(null, [sender.maximumvalue].concat(asp.DateTimePatterns));
    };

}(window));
