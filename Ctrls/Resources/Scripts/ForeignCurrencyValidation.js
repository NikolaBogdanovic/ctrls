﻿(function(asp)
{
    'use strict';
    
    window.ForeignCurrencyCompare = function(sender, args)
    {
        args.IsValid = false;
        var val1 = Number.parseLocale(args.Value);
        if(!isNaN(val1))
        {
            if(sender.operator === 'DataTypeCheck')
            {
                args.IsValid = true;
                return;
            }
            var val2;
            if(sender.controltocompare)
            {
                val2 = Number.parseLocale(asp.$get(sender.controltocompare).value);
                if(isNaN(val2))
                {
                    args.IsValid = true;
                    return;
                }
            }
            else if(sender.valuetocompare)
                val2 = Number.parseLocale(sender.valuetocompare);
            else
                return;
            switch(sender.operator)
            {
                case 'Equal':
                    args.IsValid = val1 === val2;
                    break;
                case 'NotEqual':
                    args.IsValid = val1 !== val2;
                    break;
                case 'LessThan':
                    args.IsValid = val1 < val2;
                    break;
                case 'GreaterThan':
                    args.IsValid = val1 > val2;
                    break;
                case 'LessThanEqual':
                    args.IsValid = val1 <= val2;
                    break;
                case 'GreaterThanEqual':
                    args.IsValid = val1 >= val2;
                    break;
                default:
                    throw new Error('Unknown Validation Compare Operator!');
            }
        }
    };

    window.ForeignCurrencyRange = function(sender, args)
    {
        args.IsValid = false;
        var val = Number.parseLocale(args.Value);
        if(!isNaN(val))
            args.IsValid = Number.parseLocale(sender.minimumvalue) <= val && val <= Number.parseLocale(sender.maximumvalue);
    };

}(window));
