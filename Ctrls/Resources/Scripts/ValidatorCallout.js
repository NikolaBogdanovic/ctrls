window.ValidatorCallout = function(options)
{
    'use strict';

    var asp = window;

    asp.ValidatorUpdateDisplay = function(val)
    {
        val.style.display = 'none';
    };

    options = $.extend(
        {
            calloutCss: 'ValidatorCallout',
            pointerCss: 'ValidatorPointer',
            inputCss: 'ValidatorInput',
            labelCss: 'ValidatorLabel',
            positionMy: 'left bottom',
            positionAt: 'left top',
            positionCollision: 'none none'
        },
        options);

    var callout = $('<div style="display: none; position: absolute; z-index: 1000;"></div>').appendTo(document.body);
    $('<div style="cursor: pointer;" onclick="this.parentNode.style.display = \'none\';"></div>').addClass(options.calloutCss).append($('<span></span>')).appendTo(callout);
    if(options.positionMy.indexOf('top') !== -1 || (options.positionMy.indexOf('bottom') === -1 && options.positionMy.indexOf('right') === -1))
        $('<div></div>').addClass(options.pointerCss).prependTo(callout);
    else
        $('<div></div>').addClass(options.pointerCss).appendTo(callout);

    function calloutWidth(control)
    {
        if(control.tagName.toUpperCase() === 'TEXTAREA')
            return control.offsetWidth;
        var width = 1.5 * control.offsetWidth;
        if(width < 120)
            return ((120 / control.offsetWidth) + 1) * control.offsetWidth;
        return width;
    }

    function calloutShow(control)
    {
        var isvalid = true;
        $.each(
            control.Validators,
            function(i, val)
            {
                if(isvalid && !val.isvalid)
                {
                    isvalid = false;
                    callout.find('SPAN').attr('class', val.className).html(val.innerHTML);
                    callout.width(calloutWidth(control)).show().position(
                        {
                            my: options.positionMy,
                            at: options.positionAt,
                            of: control,
                            collision: options.positionCollision
                        });
                    return false;
                }
                return true;
            });
        return !isvalid;
    }

    function updateControl(control)
    {
        var label;
        if(control.tagName.toUpperCase() === 'LABEL')
        {
            label = control;
            control = label.control || asp.$get(label.htmlFor) || $(label).find('INPUT')[0] || $(label).find('SELECT')[0] || $(label).find('TEXTAREA')[0];
        }
        else
        {
            if(control.labels && control.labels.length > 0)
                label = control.labels[0];
            else
                label = $('label[for=' + control.id + ']')[0] || $(control).siblings('LABEL')[0] || $(control.parentNode).closest('LABEL')[0];
        }
        if(calloutShow(control))
        {
            if(control.onfocus === null)
            {
                control.onfocus = function()
                {
                    calloutShow(control);
                };
                if(label)
                    asp.WebForm_AppendToClassName(label, options.labelCss);
                asp.WebForm_AppendToClassName(control, options.inputCss);
            }
        }
        else
        {
            if(control.onfocus !== null)
            {
                control.onfocus = null;
                callout.hide();
                asp.WebForm_RemoveClassName(control, options.inputCss);
                if(label)
                    asp.WebForm_RemoveClassName(label, options.labelCss);
            }
        }
    }

    var validatorOnChange = asp.ValidatorOnChange;

    asp.ValidatorOnChange = function(event)
    {
        event = event || window.event;
        validatorOnChange(event);
        updateControl(event.srcElement || event.target);
    };

    var pageClientValidate = asp.Page_ClientValidate;

    asp.Page_ClientValidate = function(validationGroup)
    {
        var isvalid = pageClientValidate(validationGroup);
        var control;
        $.each(
            asp.Page_Validators,
            function(i, val)
            {
                if(control !== val.controltovalidate)
                {
                    control = val.controltovalidate;
                    updateControl(asp.$get(control));
                }
            });
        return isvalid;
    };
};
