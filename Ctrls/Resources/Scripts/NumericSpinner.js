﻿(function(asp)
{
    'use strict';

    var original, spinning, timeout, decimalchar = asp.Sys.CultureInfo.CurrentCulture.numberFormat.NumberDecimalSeparator;
    
    window.NumericSpinner = function(id)
    {
        var span = document.createElement('span');
        span.className = 'Numeric';
        span.style.cssText = 'display: inline-block; position: relative; white-space: nowrap;';
        span.innerHTML = '<span class="Spinner" style="font-weight: bold; font-size: 50%;"><button type="button" onmousedown="SpinnerPressed(this.parentNode.previousSibling, true);" onmouseup="SpinnerReleased(this.parentNode.previousSibling);" onmouseout="SpinnerReleased(this.parentNode.previousSibling);" class="Step" style="background-color: transparent; border-width: 0; bottom: 50%; position: absolute; right: 0;">▲</button><span style="visibility: hidden;">◆</span><button type="button" onmousedown="SpinnerPressed(this.parentNode.previousSibling, false);" onmouseup="SpinnerReleased(this.parentNode.previousSibling);" onmouseout="SpinnerReleased(this.parentNode.previousSibling);" class="Step" style="background-color: transparent; border-width: 0; position: absolute; right: 0; top: 50%;">▼</button></span>';
        var input = asp.$get(id);
        input.parentNode.replaceChild(span, input);
        span.insertBefore(input, span.firstChild);
    };

    function anyStep()
    {
        var index = original.indexOf(decimalchar);
        if(index !== -1)
        {
            if(original.charAt(original.length - 1) !== decimalchar)
                ++index;
            var step = '0.';
            for(var i = 1, j = original.length - index; i < j; i++)
                step = step + '0';
            return step + '1';
        }
        return '1';
    }

    window.SpinnerPressed = function(ctrl, add)
    {
        if(ctrl.readOnly || ctrl.disabled)
            return;
        spinning = true;
        original = ctrl.value.trim();
        var step = ctrl.getAttribute('data-step');
        if(step)
        {
            step = step.trim();
            if(!step || step === 'any')
                step = anyStep();
            else
            {
                var n = Number.parseInvariant(step);
                if(isNaN(n) || n <= 0)
                    step = anyStep();
                else if(step.charAt(0) === '.')
                    step = '0' + step;
                else if(step.charAt(step.length - 1) === '.')
                    step = step + '0';
            }
        }
        else
            step = anyStep();
        var format, type = ctrl.getAttribute('data-type');
        if(type === 'number-integer')
            format = 'D';
        else if(type === 'number-double')
            format = 'g';
        else if(type === 'number-currency')
        {
            format = step.indexOf('.') + 1;
            if(format !== 0)
                format = step.length - format;
            format = 'N' + format;
        }
        else
        {
            spinning = false;
            return;
        }
        var val = ctrl.Validators;
        for(var i = 0, j = val.length; i < j; i++)
        {
            if(!val[i].minimumvalue && !val[i].maximumvalue)
                continue;
            val = val[i];
            break;
        }
        stepInterval(ctrl, (add ? 1 : -1) * Number.parseInvariant(step), Number.parseLocale(val.minimumvalue || Number.MIN_VALUE), Number.parseLocale(val.maximumvalue || Number.MAX_VALUE), format, 500, 1);
    };

    function stepInterval(ctrl, step, min, max, format, wait, i)
    {
        var old = Number.parseLocale(ctrl.value);
        var val = old;
        if(isNaN(val))
            val = 0;
        val = Math.min(Math.max(val + step, min), max);
        ctrl.value = val.localeFormat(format);
        if(val !== old && spinning)
            timeout = setTimeout(
                function()
                {
                    if(i === 90)
                        stepInterval(ctrl, step * 10, min, max, format, 200, 10);
                    else
                        stepInterval(ctrl, step, min, max, format, Math.max(10, wait - (wait / 10)), ++i);
                },
                wait);
    }

    window.SpinnerReleased = function(ctrl)
    {
        if(spinning)
        {
            spinning = false;
            clearTimeout(timeout);
            if(ctrl.value.trim() !== original)
                window.TriggerChange(ctrl);
        }
    };

}(window));
