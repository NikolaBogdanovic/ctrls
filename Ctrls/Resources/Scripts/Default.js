﻿(function(asp, page)
{
    'use strict';
    
    window.pageLoad = function(sender, args)
    {
        if(!args.get_isPartialLoad())
        {
            asp.prm = asp.prm || asp.Sys.WebForms.PageRequestManager.getInstance();
            asp.prm.add_initializeRequest(initializeRequest);
            asp.prm.add_pageLoading(pageLoading);
            asp.prm.add_pageLoaded(pageLoaded);
            asp.prm.add_endRequest(endRequest);
        }
    };

    function initializeRequest(sender, args)
    {
        asp.prm = asp.prm || asp.Sys.WebForms.PageRequestManager.getInstance();
        if(!asp.prm.get_isInAsyncPostBack())
            page.ShowProgress();
        else
            args.set_cancel(true);
    }

    function pageLoading(sender, args)
    {
        page.HideProgress();
        var panels = args.get_panelsUpdating();
        for(var i = 0, j = panels.length; i < j; i++)
            asp.WebForm_AppendToClassName(panels[i], 'Transparent');
    }

    function pageLoaded(sender, args)
    {
        var panels = args.get_panelsUpdated();
        setTimeout(
            function()
            {
                for(var i = 0, j = panels.length; i < j; i++)
                    asp.WebForm_RemoveClassName(panels[i], 'Transparent');
            },
            200);
    }

    function endRequest(sender, args)
    {
        if(args.get_error())
        {
            var error = args.get_error().message || args.get_error();
            args.set_errorHandled(true);
            page.HideProgress();
            page.Fail(window.onerror ? error : null);
        }
    }

    window.AbortPostBack = function()
    {
        asp.prm = asp.prm || asp.Sys.WebForms.PageRequestManager.getInstance();
        if(asp.prm.get_isInAsyncPostBack())
            asp.prm.abortPostBack();
        page.HideProgress();
    };

    window.DragStart = function(event)
    {
        event.dataTransfer.setData('id', (event.srcElement || event.target).id);
        event.dataTransfer.setData('cx', event.clientX);
        event.dataTransfer.setData('cy', event.clientY);
    };

    window.DragOver = function(event)
    {
        event.preventDefault();
        return false;
    };

    window.DragDrop = function(event)
    {
        var id = event.dataTransfer.getData('id');
        if(id)
        {
            event.preventDefault();
            var ctrl = asp.$get(id);
            ctrl.style.left = (Number.parseInvariant(ctrl.style.left.slice(0, -2)) + event.clientX - Number.parseInvariant(event.dataTransfer.getData('cx'))) + 'px';
            ctrl.style.top = (Number.parseInvariant(ctrl.style.top.slice(0, -2)) + event.clientY - Number.parseInvariant(event.dataTransfer.getData('cy'))) + 'px';
            return false;
        }
        return true;
    };

    window.TriggerChange = function(ctrl)
    {
        if(ctrl.dispatchEvent)
        {
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, false);
            ctrl.dispatchEvent(event);
        }
        else if(ctrl.fireEvent)
            ctrl.fireEvent('onchange', ctrl);
        else if(ctrl.onchange === 'function')
            ctrl.onchange();
    };

    window.DefaultButton = function(event, sender, target)
    {
        event = event || window.event;
        if(event.keyCode === 13)
        {
            var src = event.srcElement || event.target;
            if(src && (!src.onchange || src.onchange.toString().indexOf('__doPostBack') === -1))
            {
                var tag = src.tagName.toUpperCase();
                if(tag !== 'A' && tag !== 'TEXTAREA' && tag !== 'BUTTON' && (tag !== 'INPUT' || (src.type.toUpperCase() !== 'BUTTON' && src.type.toUpperCase() !== 'RESET' && src.type.toUpperCase() !== 'SUBMIT')))
                {
                    asp.$get(sender.id ? window.NamingContainer(sender.id) + target : target, sender).click();
                    event.cancelBubble = true;
                    if(event.stopPropagation)
                        event.stopPropagation();
                    return false;
                }
            }
        }
        return true;
    };

    window.EscapeButton = function(event, btn)
    {
        if(event.keyCode === 27)
        {
            btn.click();
            event.cancelBubble = true;
            if(event.stopPropagation)
                event.stopPropagation();
            return false;
        }
        return true;
    };

    window.TabIndex = function(event, sender)
    {
        if(event.keyCode === 9 && ((!sender && !event.shiftKey) || (sender && event.shiftKey && sender === document.activeElement)))
        {
            event.cancelBubble = true;
            if(event.stopPropagation)
                event.stopPropagation();
            return false;
        }
        return true;
    };

    window.NamingContainer = function(id)
    {
        return id.slice(0, id.lastIndexOf('_') + 1);
    };

    window.PopUp = function(ctrl, toggle)
    {
        window.onkeydown = function(event)
        {
            window.EscapeButton(event, toggle);
        };
        var vendor = null;
        if('hidden' in document)
            vendor = '';
        else if('khtmlHidden' in document)
            vendor = 'khtml';
        else if('mozHidden' in document)
            vendor = 'moz';
        else if('msHidden' in document)
            vendor = 'ms';
        else if('oHidden' in document)
            vendor = 'o';
        else if('webkitHidden' in document)
            vendor = 'webkit';
        if((vendor !== null && !document[vendor ? vendor + 'Hidden' : 'hidden']) || document.hasFocus())
            window.OnFocus(ctrl);
        else if('onfocus' in window)
        {
            window.onfocus = function()
            {
                window.onfocus = null;
                window.OnFocus(ctrl);
            };
        }
        else if('onfocusin' in document)
        {
            document.onfocusin = function()
            {
                document.onfocusin = null;
                window.OnFocus(ctrl);
            };
        }
        else if('ondomfocusin' in document)
        {
            document.ondomfocusin = function()
            {
                document.ondomfocusin = null;
                window.OnFocus(ctrl);
            };
        }
        else if('onpageshow' in window)
        {
            window.onpageshow = function()
            {
                window.onpageshow = null;
                window.OnFocus(ctrl);
            };
        }
    };

    window.OnFocus = function(ctrl)
    {
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var offsetWidth = width < ctrl.offsetWidth ? (width - (width / 5)) : ctrl.offsetWidth;
        var offsetHeight = height < ctrl.offsetHeight ? (height - (height / 5)) : ctrl.offsetHeight;
        ctrl.style.left = ((width - offsetWidth) / 2) + 'px';
        ctrl.style.top = ((height - offsetHeight) / 2) + 'px';
        ctrl.focus();
    };
    
    window.CloseDown = function(sender, toggle)
    {
        if(sender)
        {
            var vg = window.ValidationGroup(sender);
            if(vg)
            {
                asp.Page_ClientValidate(vg);
                if(!asp.Page_IsValid)
                    return false;
            }
        }
        toggle.click();
        return sender ? true : false;
    };

    window.ResetControls = function(ctrls)
    {
        for(var i = 0, j = ctrls.length; i < j; i++)
        {
            var c = ctrls[i];
            if(c.disabled)
                continue;
            var tag = c.tagName;
            if(!tag)
                continue;
            tag = tag.toUpperCase();
            if(tag === 'INPUT')
            {
                var type = c.type.toUpperCase();
                if(type === 'CHECKBOX' || type === 'RADIO')
                        c.checked = false;
                else if(type !== 'BUTTON' && type !== 'HIDDEN' && type !== 'IMAGE' && type !== 'RESET' && type !== 'SUBMIT')
                        c.value = '';
            }
            else if(tag === 'TEXTAREA')
                    c.value = '';
            else if(tag === 'SELECT')
            {
                for(var k = 0, l = c.length; k < l; k++)
                    c.options[k].selected = false;
            }
            else if(c.hasChildNodes())
                window.ResetControls(c.childNodes);
        }
    };

    window.ResetForm = function(sender, form, submit)
    {
        var id = window.NamingContainer(sender.id);
        window.ResetControls(asp.$get(id + form).childNodes);
        submit = asp.$get(id + submit);
        var vg = window.ValidationGroup(submit);
        if(vg)
        {
            asp.Page_ClientValidate(vg);
            if(!asp.Page_IsValid)
                return;
        }
        submit.click();
    };

    window.ValidationGroup = function(sender)
    {
        var vg = sender.getAttribute('onclick');
        if(vg)
        {
            var i = vg.indexOf('Page_ClientValidate');
            if(i !== -1)
            {
                i += 21;
                return vg.slice(i, vg.indexOf('\'', i));
            }
        }
        return null;
    };

    window.DoubleDelegate = function(func1, func2)
    {
        return function()
        {
            if(func1)
                func1();
            if(func2)
                func2();
        };
    };

}(window, window.page));
