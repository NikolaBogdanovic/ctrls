﻿namespace Ctrls.Providers
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Web.Security.AntiXss;

    /// <summary>
    /// Provides encoding and decoding logic for use in HTML, XML, CSS, and URL strings.
    /// </summary>
    /// <remarks>
    /// Outputs JavaScript compatible strings, by reverting <c>&amp;#13;&amp;#10;</c> back to new line, <c>&amp;#32;</c> to space character, and <c>&amp;#39;</c> to single quote.
    /// <para></para>
    /// Register in "Web.config" file: <c>&lt;httpRuntime encoderType="Utils.Types.JavaScriptCompatibleHttpEncoder" /&gt;</c>
    /// </remarks>
    public sealed class JavaScriptCompatibleHttpEncoder : AntiXssEncoder
    {
        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override void HtmlAttributeEncode(string value, TextWriter output)
        {
            var sb = new StringBuilder();
            base.HtmlAttributeEncode(value, new StringWriter(sb, CultureInfo.InvariantCulture));
            output.Write(sb.Replace("&#13;&#10;", Environment.NewLine).Replace("&#32;", " ").Replace("&#39;", "'").ToString());
        }

        /// <inheritdoc/>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        protected override void HtmlEncode(string value, TextWriter output)
        {
            var sb = new StringBuilder();
            base.HtmlEncode(value, new StringWriter(sb, CultureInfo.CurrentCulture));
            output.Write(sb.Replace("&#13;&#10;", Environment.NewLine).Replace("&#32;", " ").Replace("&#39;", "'").ToString());
        }
    }
}
