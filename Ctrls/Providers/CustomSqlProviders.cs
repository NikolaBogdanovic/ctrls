﻿namespace Ctrls.Providers
{
    using System.Collections.Specialized;
    using System.Web.Profile;
    using System.Web.Security;

    using Utils;

    /// <summary>
    /// Custom SQL Server based membership provider, using dynamic connection string.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add type="Ctrls.Providers.CustomSqlMembershipProvider" /&gt;</c>
    /// </remarks>
    public sealed class CustomSqlMembershipProvider : SqlMembershipProvider
    {
        /// <inheritdoc/>
        public override void Initialize(string name, NameValueCollection config)
        {
            config["connectionString"] = Config.ConnectionString;
            config.Remove("connectionStringName");
            base.Initialize(name, config);
        }
    }

    /// <summary>
    /// Custom SQL Server based profile provider, using dynamic connection string.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add type="Ctrls.Providers.CustomSqlProfileProvider" /&gt;</c>
    /// </remarks>
    public sealed class CustomSqlProfileProvider : SqlProfileProvider
    {
        /// <inheritdoc/>
        public override void Initialize(string name, NameValueCollection config)
        {
            config["connectionString"] = Config.ConnectionString;
            config.Remove("connectionStringName");
            base.Initialize(name, config);
        }
    }

    /// <summary>
    /// Custom SQL Server based role provider, using dynamic connection string.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add type="Ctrls.Providers.CustomSqlRoleProvider" /&gt;</c>
    /// </remarks>
    public sealed class CustomSqlRoleProvider : SqlRoleProvider
    {
        /// <inheritdoc/>
        public override void Initialize(string name, NameValueCollection config)
        {
            config["connectionString"] = Config.ConnectionString;
            config.Remove("connectionStringName");
            base.Initialize(name, config);
        }
    }
}
