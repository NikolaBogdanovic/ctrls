﻿namespace Ctrls.Providers
{
    using System.CodeDom;
    using System.Web.Compilation;
    using System.Web.UI;

    /// <summary>
    /// Evaluates code expressions, in web control attribute/property value markup declarations, during page parsing.
    /// </summary>
    /// <remarks>
    /// Executes code snippets on the <see cref="System.Web.UI.Control.Init"/> event - before the <see cref="PageStatePersister.ViewState"/> is tracking changes.
    /// <para></para>
    /// Register in "Web.config" file:
    /// <code> 
    /// &lt;compilation&gt;
    ///     &lt;expressionBuilders&gt;
    ///        &lt;add expressionPrefix="Code" type="Utils.Types.CodeExpressionBuilder" /&gt;
    ///    &lt;/expressionBuilders&gt;
    /// &lt;/compilation&gt;
    /// </code> 
    /// Usage: <c>&lt;asp:Literal runat="server" Text='&lt;%$ Code: ScriptManager.GetCurrent(Page).IsInAsyncPostBack %&gt;' Visible='&lt;%$ Code: Page.IsPostBack %&gt;' /&gt;</c>
    /// </remarks>
    [ExpressionPrefix("Code")]
    public sealed class CodeExpressionBuilder : ExpressionBuilder
    {
        /// <inheritdoc/>
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        {
            return new CodeSnippetExpression(entry.Expression);
        }
    }
}
