﻿namespace Ctrls.Modules
{
    using System;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Utils;

    /// <summary>
    /// Global.asax
    /// </summary>
    /// <remarks>
    /// Register in "Global.asax" file: <c>&lt;%@ Application Language="C#" Inherits="Ctrls.Modules.GlobalAsax" %&gt;</c>
    /// </remarks>
    public class GlobalAsax : HttpApplication
    {
        private static readonly object EventApplicationStart = new object();

        /// <summary>
        /// Occurs when the application starts.
        /// </summary>
        public event EventHandler ApplicationStart
        {
            add
            {
                Events.AddHandler(EventApplicationStart, value);
            }
            remove
            {
                Events.RemoveHandler(EventApplicationStart, value);
            }
        }

        /// <summary>
        /// Raises the <see cref="ApplicationStart"/> event when the application starts.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnApplicationStart(EventArgs e)
        {
            var handler = Events[EventApplicationStart] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Occurs when the application starts.
        /// </summary>
        /// <remarks>
        /// Special method, and not an application event, so can't be handled in a module. Use the <see cref="ApplicationStart"/> event instead.
        /// </remarks>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        protected virtual void Application_OnStart(object sender, EventArgs e)
        {
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.DefaultThreadCurrentCulture = Config.ServerCulture;
            ScriptManager.ScriptResourceMapping.AddDefinition(
                "jquery",
                new ScriptResourceDefinition
                {
                    Path = Properties.Settings.Default.ScriptsPath + "jQuery/jquery-1.8.3.min.js",
                    DebugPath = Properties.Settings.Default.ScriptsPath + "jQuery/jquery-1.8.3.js",
                    CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js",
                    CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.js",
                    CdnSupportsSecureConnection = true,
                    LoadSuccessExpression = "window.jQuery"
                });
            WebControl.DisabledCssClass = "Disabled";
            OnApplicationStart(e);
        }

        private static readonly object EventApplicationEnd = new object();

        /// <summary>
        /// Occurs when the application ends.
        /// </summary>
        public event EventHandler ApplicationEnd
        {
            add
            {
                Events.AddHandler(EventApplicationEnd, value);
            }
            remove
            {
                Events.RemoveHandler(EventApplicationEnd, value);
            }
        }

        /// <summary>
        /// Raises the <see cref="ApplicationEnd"/> event when the application ends.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnApplicationEnd(EventArgs e)
        {
            var handler = Events[EventApplicationEnd] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Occurs when the application ends.
        /// </summary>
        /// <remarks>
        /// Special method, and not an application event, so can't be handled in a module. Use the <see cref="ApplicationEnd"/> event instead.
        /// </remarks>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        protected virtual void Application_OnEnd(object sender, EventArgs e)
        {
            OnApplicationEnd(e);
        }

        private static readonly object EventSessionEnd = new object();

        /// <summary>
        /// Occurs when a session ends.
        /// </summary>
        public event EventHandler SessionEnd
        {
            add
            {
                Events.AddHandler(EventSessionEnd, value);
            }
            remove
            {
                Events.RemoveHandler(EventSessionEnd, value);
            }
        }

        /// <summary>
        /// Raises the <see cref="SessionEnd"/> event when a session ends.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnSessionEnd(EventArgs e)
        {
            var handler = Events[EventSessionEnd] as EventHandler;
            if(handler != null)
                handler(this, e);
        }

        /// <summary>
        /// Occurs when a session ends.
        /// </summary>
        /// <remarks>
        /// Special <see cref="System.Web.SessionState.SessionStateModule.End"/> event, and can't be handled in a module. Use the <see cref="SessionEnd"/> event instead.
        /// </remarks>
        /// <param name="sender">The event source.</param>
        /// <param name="e">The event data.</param>
        protected virtual void Session_OnEnd(object sender, EventArgs e)
        {
            OnSessionEnd(e);
        }
    }
}
