﻿namespace Ctrls.Modules
{
    using System;
    using System.Collections.Specialized;
    using System.Web;

    /// <summary>
    /// Removes "Server" header from response, to hide the target software from a potential attacker.
    /// Removes "ETag" header from response, for cache to threat the same files from different web servers as one.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add name="Headers" type="Ctrls.Modules.Headers" /&gt;</c>
    /// </remarks>
    public sealed class Headers : IHttpModule
    {
        /// <inheritdoc/>
        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += OnPreSendRequestHeaders;
        }

        private static void OnPreSendRequestHeaders(object sender, EventArgs e)
        {
            NameValueCollection headers = ((HttpApplication)sender).Response.Headers;
            headers.Remove("Server");
            headers.Remove("ETag");
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
