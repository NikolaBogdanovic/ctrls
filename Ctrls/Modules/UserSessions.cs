﻿namespace Ctrls.Modules
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Runtime.Caching;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;
    using System.Web.SessionState;

    using Utils;

    /// <summary>
    /// Tracks active user sessions, failed authentication attempts, and currently authenticated users.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add name="UserSessions" type="Ctrls.Modules.UserSessions" preCondition="managedHandler" /&gt;</c>
    /// </remarks>
    public sealed class UserSessions : IHttpModule, IReadOnlySessionState
    {
        /// <inheritdoc/>
        public void Init(HttpApplication context)
        {
            context.PostAuthorizeRequest += OnPostAuthorizeRequest;
            context.PostAcquireRequestState += OnPostAcquireRequestState;
            ((GlobalAsax)context).SessionEnd += OnSessionEnd;
        }

        private static void OnPostAuthorizeRequest(object sender, EventArgs e)
        {
            IIdentity user = ((HttpApplication)sender).User.Identity;
            if(user.IsAuthenticated)
            {
                MemoryCache cache = MemoryCache.Default;
                if(cache["User: " + user.Name] == null)
                {
                    cache.Add(
                        "User: " + user.Name,
                        Roles.Enabled ? Roles.GetRolesForUser() : CookieRoles.GetRolesForUser(),
                        new CacheItemPolicy
                        {
                            AbsoluteExpiration = FormsAuthentication.SlidingExpiration ? MemoryCache.InfiniteAbsoluteExpiration : ((FormsIdentity)user).Ticket.Expiration.ToUniversalTime(),
                            Priority = CacheItemPriority.NotRemovable,
                            SlidingExpiration = FormsAuthentication.SlidingExpiration ? FormsAuthentication.Timeout : MemoryCache.NoSlidingExpiration
                        });
                    cache.Remove(UsersKey);
                }
            }
        }

        private static void OnPostAcquireRequestState(object sender, EventArgs e)
        {
            MemoryCache cache = MemoryCache.Default;
            HttpSessionState session = ((HttpApplication)sender).Context.Session;
            if(session != null && cache["Session: " + session.SessionID] == null)
            {
                ResetFailedAuthenticationCount(session.SessionID);
                cache.Remove(SessionsKey);
            }
        }

        /// <summary>
        /// Resets the failed authentication count.
        /// </summary>
        /// <param name="id">The session identifier.</param>
        public static void ResetFailedAuthenticationCount(string id)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(id));

            MemoryCache.Default.Set(
                "Session: " + id,
                0L,
                new CacheItemPolicy
                {
                    Priority = CacheItemPriority.NotRemovable,
                    SlidingExpiration = Config.SlidingExpiration
                });
        }

        /// <summary>
        /// Increments the failed authentication count, or sets the number of ticks that represent the date and time of the last allowed attempt.
        /// </summary>
        /// <param name="id">The session identifier.</param>
        /// <param name="max">The maximum number of allowed attempts.</param>
        public static void IncrementFailedAuthenticationCount(string id, byte max)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(id));
            Contract.Requires(max > 0);

            MemoryCache cache = MemoryCache.Default;
            long attempts = (cache["Session: " + id] as long? ?? 0) + 1;
            if(attempts > max)
                return;
            cache.Set(
                "Session: " + id,
                attempts < max ? attempts : Http.CurrentTimestamp.Ticks,
                new CacheItemPolicy
                {
                    Priority = CacheItemPriority.NotRemovable,
                    SlidingExpiration = Config.SlidingExpiration
                });
        }

        /// <summary>
        /// Removes the forms authentication ticket.
        /// </summary>
        public static void SignOut()
        {
            FormsAuthentication.SignOut();
            MemoryCache cache = MemoryCache.Default;
            cache.Remove("User: " + HttpContext.Current.User.Identity.Name);
            cache.Remove(UsersKey);
        }

        /// <summary>
        /// Cancels the current session.
        /// </summary>
        public static void Abandon()
        {
            var context = HttpContext.Current;
            context.Session.Abandon();
            MemoryCache cache = MemoryCache.Default;
            cache.Remove("Session: " + context.Session.SessionID);
            cache.Remove(SessionsKey);
        }

        private static void OnSessionEnd(object sender, EventArgs e)
        {
            MemoryCache cache = MemoryCache.Default;
            cache.Remove("Session: " + ((HttpApplication)sender).Session.SessionID);
            cache.Remove(SessionsKey);
        }

        // ReSharper disable SuggestBaseTypeForParameter
        private static void Cache(List<string> sessions, List<string> users)
        // ReSharper restore SuggestBaseTypeForParameter
        {
            var keys = new List<string>();
            MemoryCache cache = MemoryCache.Default;
            foreach(KeyValuePair<string, object> kvp in cache)
                keys.Add(kvp.Key);
            foreach(string s in keys)
            {
                if(s.StartsWith("Session: ", StringComparison.Ordinal))
                    sessions.Add(s.Substring(9));
                else if(s.StartsWith("User: ", StringComparison.Ordinal))
                    users.Add(s.Substring(6));
            }
            DateTime expiration = Http.CurrentTimestamp.Add(Config.SlidingExpiration);
            cache.Set(
                SessionsKey,
                sessions,
                new CacheItemPolicy
                {
                    AbsoluteExpiration = expiration,
                    Priority = CacheItemPriority.NotRemovable
                });
            cache.Set(
                UsersKey,
                users,
                new CacheItemPolicy
                {
                    AbsoluteExpiration = expiration,
                    Priority = CacheItemPriority.NotRemovable
                });
        }

        private const string SessionsKey = "ActiveSessions";

        private static readonly object SessionsLock = new object();

        /// <summary>
        /// Gets the currently active session identifiers.
        /// </summary>
        public static IReadOnlyList<string> ActiveSessions
        {
            get
            {
                MemoryCache cache = MemoryCache.Default;
                var list = cache[SessionsKey] as List<string>;
                if(list == null)
                {
                    lock(SessionsLock)
                    {
                        list = cache[SessionsKey] as List<string>;
                        if(list == null)
                        {
                            list = new List<string>();
                            Cache(list, new List<string>());
                        }
                    }
                }
                return list;
            }
        }

        private const string UsersKey = "AuthenticatedUsers";

        private static readonly object UsersLock = new object();

        /// <summary>
        /// Gets the currently authenticated user names.
        /// </summary>
        public static IReadOnlyList<string> AuthenticatedUsers
        {
            get
            {
                MemoryCache cache = MemoryCache.Default;
                var list = cache[UsersKey] as List<string>;
                if(list == null)
                {
                    lock(UsersLock)
                    {
                        list = cache[UsersKey] as List<string>;
                        if(list == null)
                        {
                            list = new List<string>();
                            Cache(new List<string>(), list);
                        }
                    }
                }
                return list;
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
