﻿namespace Ctrls.Modules
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;

    using Utils;

    /// <summary>
    /// Cookie based roles authorization.
    /// </summary>
    /// <remarks>
    /// Do not use with any role provider.
    /// <para></para>
    /// Register in "Web.config" file: <c>&lt;add name="CookieRoles" type="Ctrls.Modules.CookieRoles" preCondition="managedHandler" /&gt;</c>
    /// </remarks>
    public sealed class CookieRoles : IHttpModule
    {
        /// <inheritdoc/>
        public void Init(HttpApplication context)
        {
            ((FormsAuthenticationModule)context.Modules["FormsAuthentication"]).Authenticate += OnAuthenticate;
        }

        private static void OnAuthenticate(object sender, FormsAuthenticationEventArgs e)
        {
            HttpCookie cookie = e.Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if(cookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                if(ticket != null && !ticket.Expired)
                {
                    if(FormsAuthentication.SlidingExpiration)
                    {
                        FormsAuthenticationTicket renew = FormsAuthentication.RenewTicketIfOld(ticket);
                        if(renew != null && renew != ticket)
                        {
                            ticket = renew;
                            cookie.Domain = FormsAuthentication.CookieDomain;
                            if(ticket.IsPersistent)
                                cookie.Expires = ticket.Expiration.ToUniversalTime();
                            cookie.HttpOnly = true;
                            cookie.Name = FormsAuthentication.FormsCookieName;
                            cookie.Path = FormsAuthentication.FormsCookiePath;
                            cookie.Secure = FormsAuthentication.RequireSSL;
                            cookie.Value = FormsAuthentication.Encrypt(ticket);
                            e.Context.Response.Cookies.Add(cookie);
                        }
                    }
                    e.User = new GenericPrincipal(new FormsIdentity(ticket), ticket.UserData.Split('|'));
                }
            }
        }

        /// <summary>
        /// Creates a custom forms authentication ticket, with authorized user roles, and adds it to a cookie.
        /// </summary>
        /// <param name="userName">The authenticated user name.</param>
        /// <param name="createPersistentCookie"><c>true</c> to create a durable cookie (one that is saved across browser sessions); otherwise, <c>false</c>.</param>
        /// <param name="roleNames">(Optional) the authorized roles list.</param>
        /// <returns>
        /// A cookie.
        /// </returns>
        public static HttpCookie GetAuthCookie(string userName, bool createPersistentCookie, string[] roleNames = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(userName));

            DateTime issue = Http.CurrentTimestamp;
            DateTime expiration = issue.Add(FormsAuthentication.Timeout);
            var ticket = new FormsAuthenticationTicket(4, userName, issue, expiration, createPersistentCookie, roleNames != null ? string.Join("|", roleNames) : string.Empty, FormsAuthentication.FormsCookiePath);
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket))
            {
                Domain = FormsAuthentication.CookieDomain,
                HttpOnly = true,
                Path = FormsAuthentication.FormsCookiePath,
                Secure = FormsAuthentication.RequireSSL
            };
            if(ticket.IsPersistent)
                cookie.Expires = ticket.Expiration.ToUniversalTime();
            return cookie;
        }

        /// <summary>
        /// Creates a custom forms authentication ticket, with authorized user roles, and adds it to the cookies collection of the response.
        /// </summary>
        /// <param name="userName">The authenticated user name.</param>
        /// <param name="createPersistentCookie"><c>true</c> to create a durable cookie (one that is saved across browser sessions); otherwise, <c>false</c>.</param>
        /// <param name="roleNames">(Optional) the authorized roles list.</param>
        public static void SetAuthCookie(string userName, bool createPersistentCookie, string[] roleNames = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(userName));

            Http.CurrentResponse.Cookies.Add(GetAuthCookie(userName, createPersistentCookie, roleNames));
        }

        /// <summary>
        /// Creates a custom forms authentication ticket, with authorized user roles, and adds it to the cookies collection of the response. 
        /// Redirects back to the originally requested URL, or the default URL.
        /// </summary>
        /// <param name="userName">The authenticated user name.</param>
        /// <param name="createPersistentCookie"><c>true</c> to create a durable cookie (one that is saved across browser sessions); otherwise, <c>false</c>.</param>
        /// <param name="roleNames">(Optional) the authorized roles list.</param>
        public static void RedirectFromLoginPage(string userName, bool createPersistentCookie, string[] roleNames = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(userName));

            SetAuthCookie(userName, createPersistentCookie, roleNames);
            Http.CurrentResponse.Redirect(FormsAuthentication.GetRedirectUrl(userName, true), false);
        }

        /// <summary>
        /// Gets the authorized roles list for the currently authenticated user.
        /// </summary>
        /// <returns>
        /// The authorized roles list.
        /// </returns>
        public static string[] GetRolesForUser()
        {
            IIdentity user = HttpContext.Current.User.Identity;
            if(user.IsAuthenticated)
                return ((FormsIdentity)user).Ticket.UserData.Split('|');
            return new string[0];
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
