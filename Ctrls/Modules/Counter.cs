﻿namespace Ctrls.Modules
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Web;
    using System.Web.SessionState;

    using Utils;
    using Utils.Types;

    /// <summary>
    /// Counts unique visitors, total visits, page views, and site hits.
    /// </summary>
    /// <remarks>
    /// Non-locking and thread-safe, with a fail-over backup.
    /// <para></para>
    /// Register in "Web.config" file: <c>&lt;add name="Counter" type="Ctrls.Modules.Counter" /&gt;</c>
    /// </remarks>
    public sealed class Counter : IHttpModule
    {
        /// <inheritdoc/>
        public void Init(HttpApplication context)
        {
            var asax = (GlobalAsax)context;
            asax.ApplicationStart += OnApplicationStart;
            context.BeginRequest += OnBeginRequest;
            context.PostMapRequestHandler += OnPostMapRequestHandler;
            ((SessionStateModule)context.Modules["Session"]).Start += OnSessionStart;
            asax.ApplicationEnd += OnApplicationEnd;
        }

        private static readonly SemaphoreSlim OriginalWriterLock = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim OriginalReaderLock = new SemaphoreSlim(int.MaxValue);
        private static readonly SemaphoreSlim BackupWriterLock = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim BackupReaderLock = new SemaphoreSlim(int.MaxValue);

        private static readonly FileReadData Original = new FileReadData(Path.Combine(Config.DataPath, "Counter.txt"), OriginalReaderLock, OriginalWriterLock);
        private static readonly FileCopyData Backup = new FileCopyData(Original, new FileReadData(Path.Combine(Config.DataPath, "Backups", "Counter.txt"), BackupReaderLock, BackupWriterLock), false, true);

        private static long uniqueVisitors;

        /// <summary>
        /// Gets the unique visitors count.
        /// </summary>
        public static long UniqueVisitors
        {
            get
            {
                return Interlocked.Read(ref uniqueVisitors);
            }
        }

        private static long totalVisits;

        /// <summary>
        /// Gets the total visits count.
        /// </summary>
        public static long TotalVisits
        {
            get
            {
                return Interlocked.Read(ref totalVisits);
            }
        }

        private static long pageViews;

        /// <summary>
        /// Gets the page views count.
        /// </summary>
        public static long PageViews
        {
            get
            {
                return Interlocked.Read(ref pageViews);
            }
        }

        private static long siteHits;

        /// <summary>
        /// Gets the site hits count.
        /// </summary>
        public static long SiteHits
        {
            get
            {
                return Interlocked.Read(ref siteHits);
            }
        }

        private static void Count(FileReadData file, CancellationToken cancel)
        {
            string[] counts = FileIO.ReadFile(file, cancel).Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            uniqueVisitors = long.Parse(counts[0].Substring(counts[0].LastIndexOf(' ') + 1), NumberStyles.None, CultureInfo.InvariantCulture);
            totalVisits = long.Parse(counts[1].Substring(counts[1].LastIndexOf(' ') + 1), NumberStyles.None, CultureInfo.InvariantCulture);
            pageViews = long.Parse(counts[2].Substring(counts[2].LastIndexOf(' ') + 1), NumberStyles.None, CultureInfo.InvariantCulture);
            siteHits = long.Parse(counts[3].Substring(counts[3].LastIndexOf(' ') + 1), NumberStyles.None, CultureInfo.InvariantCulture);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void OnApplicationStart(object sender, EventArgs e)
        {
            CancellationToken cancel = Helper.CancelToken();
            try
            {
                Count(Original, cancel);
            }
            catch
            {
                Count(Backup.Copy, cancel);
                FileIO.CopyFile(new FileCopyData(Backup.Copy, Original, false, true), cancel);
            }
        }

        private static void Save()
        {
            RegisteredTask.Start(FileIO.WriteFileAsync(new FileWriteData(Original, string.Format(CultureInfo.InvariantCulture, "Unique Visitors: {1}{0}Total Visits: {2}{0}Page Views: {3}{0}Site Hits: {4}{0}", Environment.NewLine, UniqueVisitors, TotalVisits, PageViews, SiteHits), false), RegisteredTask.CancelToken));
        }

        private static void OnBeginRequest(object sender, EventArgs e)
        {
            Interlocked.Increment(ref siteHits);
            Save();
        }

        private static void OnPostMapRequestHandler(object sender, EventArgs e)
        {
            if(((HttpApplication)sender).Context.Handler != null)
            {
                Interlocked.Increment(ref pageViews);
                Save();
            }
        }

        private static void OnSessionStart(object sender, EventArgs e)
        {
            Interlocked.Increment(ref totalVisits);
            var req = Http.CurrentRequest;
            if(req != null && req.Cookies["Profile"] == null)
                Interlocked.Increment(ref uniqueVisitors);
            Save();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private static void OnApplicationEnd(object sender, EventArgs e)
        {
            try
            {
                Count(Original, CancellationToken.None);
            }
            catch
            {
                return;
            }
            RegisteredTask.Start(FileIO.CopyFileAsync(Backup, CancellationToken.None));
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
