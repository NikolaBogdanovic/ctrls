﻿namespace Ctrls.Modules
{
    using System;
    using System.Threading.Tasks;
    using System.Web;

    using Utils;
    using Utils.Types;

    /// <summary>
    /// Catches, observes, and logs unhandled exceptions.
    /// </summary>
    /// <remarks>
    /// Register in "Web.config" file: <c>&lt;add name="Errors" type="Ctrls.Modules.Errors" /&gt;</c>
    /// </remarks>
    public sealed class Errors : IHttpModule
    {
        /// <inheritdoc/>
        public void Init(HttpApplication context)
        {
            ((GlobalAsax)context).ApplicationStart += OnStart;
            context.Error += OnError;
        }

        private static void OnStart(object sender, EventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException;
        }

        private static void OnError(object sender, EventArgs e)
        {
            var app = (HttpApplication)sender;
            Exception ex = app.Server.GetLastError();
            if(ex != null)
            {
                if(!app.Context.IsDebuggingEnabled)
                {
                    ex = ex.GetBaseException();
                    var http = ex as HttpException;
                    if(http != null)
                    {
                        if(http is HttpRequestValidationException)
                        {
                            RegisteredTask.Start(Logger.LogExceptionAsync(true, ex, app.Request));
                            app.Server.ClearError();
                            app.Response.Clear();
                            throw new HttpException(400, "400 Bad Request");
                        }
                        int code = http.GetHttpCode();
                        if((code == 400 || code == 403 || code == 404) && app.Request.Browser.Crawler)
                            return;
                    }
                    else if(Database.TimeOut(ex) || ex is TimeoutException || ex is OperationCanceledException)
                    {
                        RegisteredTask.Start(Logger.LogExceptionAsync(true, ex, app.Request));
                        app.Server.ClearError();
                        app.Response.Clear();
                        throw new HttpException(503, "503 Service Unavailable");
                    }
                }
                RegisteredTask.Start(Logger.LogExceptionAsync(true, ex, app.Request));
            }
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            RegisteredTask.Start(Logger.LogExceptionAsync(true, (Exception)e.ExceptionObject, Http.CurrentRequest));
        }

        private static void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if(!e.Observed)
            {
                e.SetObserved();
                RegisteredTask.Start(Logger.LogExceptionAsync(true, e.Exception, Http.CurrentRequest));
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
        }
    }
}
