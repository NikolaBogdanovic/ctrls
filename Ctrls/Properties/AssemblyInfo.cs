﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("8bde3d12-da04-48c9-8aa5-9e22aba82c9a")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDefaultAlias("Custom Server Controls")]
[assembly: AssemblyTitle("Custom Server Controls")]
[assembly: AssemblyDescription("Custom Server Controls")]
[assembly: AssemblyProduct("Custom Server Controls")]
[assembly: AssemblyCompany("Nikola Bogdanović")]
[assembly: AssemblyCopyright("© 2012-2015 http://rs.linkedin.com/in/NikolaBogdanovic")]
[assembly: AssemblyTrademark("pOcHa")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
