﻿namespace Ctrls.Properties
{
    using System.Configuration;

    /// <exclude/>
    #pragma warning disable 1591
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Settings : ApplicationSettingsBase
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly Settings Default = (Settings)Synchronized(new Settings());
        
        [ApplicationScopedSetting]
        public string ScriptsPath
        {
            get
            {
                return (string)this["ScriptsPath"] ?? "/static/scripts/";
            }
        }

        [ApplicationScopedSetting]
        public string ValidatorsResx
        {
            get
            {
                return (string)this["ValidatorsResx"] ?? "~/Validators";
            }
        }
    }
    #pragma warning restore 1591
}
